#[============================================================================[

FindAdditionalCxxStdLibs
------------------------

Finds a required set of an additional libraries for the standard C++ library.

Examples of the additional libraries are stdc++fs for libstdc++ and c++fs for
libc++ that are requiered for <filesystem> part of the library.

Probably this is yet another package doing this :D

NOTE: This set doesn't includes threads, use find_package(Threads).

Usage:
    find_package(AdditionalCxxStdLibs)  # as usual, but no VERSION support.
    target_link_libraries(${PROJECT_NAME} ${AdditionalCxxStdLibs_LIBRARIES})

or if you need only a part of the standard library (e.g. atomic) use:
    find_package(AdditionalCxxStdLibs REQUIRED atomic ...)

Components: atomic, filesystem.

If no components are specified, than all components are searched for as
optional ones, or, if REQUIERED is specified, as required ones.

Non-cached variables to use in CMakeLists.txt:
- AdditionalCxxStdLibs_FOUND
- AdditionalCxxStdLibs_atomic_FOUND
- AdditionalCxxStdLibs_filesystem_FOUND
- AdditionalCxxStdLibs_LIBRARIES

Cached variables (not for use in CMakeLists.txt):
- AdditionalCxxStdLibs_atomic_LIBRARY
- AdditionalCxxStdLibs_filesystem_LIBRARY


HOW TO EXTEND?
This module may be extended easily:

-   To add a new library that "fixes" an existing component
    (component_name) for some compiler just append its name to
    _ADDITIONAL_CXX_STD_LIBS_VARIANTS_component_name.

-   To add new "component" with a name "new_component":
    1.  Append its name to _ADDITIONAL_CXX_STD_LIBS_COMPONENTS
    2.  Add _ADDITIONAL_CXX_STD_LIBS_VARIANTS_new_component that lists
        all variants of the additional libraries for this new component
        (for examples see the sources below).
    3.  Set _ADDITIONAL_CXX_STD_LIBS_VERSION_new_component to the required
        version of the C++ standard.
    4.  Set _ADDITIONAL_CXX_STD_LIBS_HEADER_new_component to the header name
        of that "component" (to test is the compiler supports it at all).
    5.  Write simple C++ source file that uses that "component" and produces
        link errors if isn't linked with a proper additional library from
        the step 2.
    6.  Put this source file to the "src" subdirectory of the directory
        containing this module using the name test_new_component.cpp

Author:
    2019, Parfenov Nikita
License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

#]============================================================================]

# cmake version here isn't accurate
cmake_minimum_required(VERSION 3.8)


# configuration variables for components

set(_ADDITIONAL_CXX_STD_LIBS_COMPONENTS "atomic;filesystem")

set(_ADDITIONAL_CXX_STD_LIBS_VARIANTS_atomic "atomic")
set(_ADDITIONAL_CXX_STD_LIBS_HEADER_atomic "atomic")
set(_ADDITIONAL_CXX_STD_LIBS_VERSION_atomic 11)

set(_ADDITIONAL_CXX_STD_LIBS_VARIANTS_filesystem "stdc++fs;c++fs")
set(_ADDITIONAL_CXX_STD_LIBS_HEADER_filesystem "filesystem")
set(_ADDITIONAL_CXX_STD_LIBS_VERSION_filesystem 17)


# the "generic" part of the code

include(CheckIncludeFileCXX)

set(ADDITIONAL_CXX_STD_LIBS_SRC_DIR ${CMAKE_CURRENT_LIST_DIR}/src)

function ( get_std_component_lib result_var std_header standard_version test_file lib_variants is_required is_quetly)

    set(CMAKE_REQUIRED_FLAGS "-DCMAKE_CXX_STANDARD=${standard_version}")
    set(INCLUDE_CHECK_VAR_NAME "_ADDITIONAL_CXX_STD_LIBS_HAS_STD_${std_header}_SUPPORT")

    function ( status_msg msg )
        if(NOT is_quetly)
            message(STATUS "${msg}")
        endif()
    endfunction(status_msg)

    function ( error_msg msg )
        if(is_required)
            message(FATAL_ERROR "${msg}")
        elseif(NOT is_quetly)
            message(WARNING "${msg}")
        endif()
    endfunction(error_msg)

    # Do we need to restore the state of CMAKE_REQUIRED_QUIET?
    set(CMAKE_REQUIRED_QUIET ${is_quetly})
    CHECK_INCLUDE_FILE_CXX("${std_header}" ${INCLUDE_CHECK_VAR_NAME})

    if(NOT "${${INCLUDE_CHECK_VAR_NAME}}")
        error_msg("compiler doesn't support <${std_header}>")
        return()
    endif()

    if(NOT DEFINED "${result_var}")

        # is it better to test in the reverse order?
        # i.e. to use additional library anyway if it is possible.

        function ( try_compile_helper result_var lib )
            try_compile(
                ${result_var}_${lib} ${CMAKE_BINARY_DIR} ${ADDITIONAL_CXX_STD_LIBS_SRC_DIR}/${test_file}
                CMAKE_FLAGS -DCMAKE_CXX_STANDARD=${standard_version}
                LINK_LIBRARIES ${lib}
                )
            set(${result_var} ${${result_var}_${lib}} PARENT_SCOPE)
        endfunction(try_compile_helper)

        status_msg("Test if a code that uses <${std_header}> requires a special linking")
        set(COMPILE_TEST_VAR_NAME "AdditionalCxxStdLibs_${test_file}_COMPILED")
        try_compile_helper(${COMPILE_TEST_VAR_NAME} "")

        if(${COMPILE_TEST_VAR_NAME})
            status_msg("<${std_header}> doesn't require a special linking")
            set(${result_var} "" CACHE STRING "TEST ${result_var}")
            mark_as_advanced(${result_var})
            return()
        else()
            status_msg("<${std_header}> requires a special linking")
        endif()

        foreach(lib IN LISTS lib_variants)

            status_msg("Trying to build a code that uses <${std_header}>, linking with '${lib}' library")
            try_compile_helper(${COMPILE_TEST_VAR_NAME} ${lib})

            if(${COMPILE_TEST_VAR_NAME})
                status_msg("<${std_header}> requires '${lib}' library")
                set(${result_var} ${lib} CACHE STRING "TEST ${result_var}")
                mark_as_advanced(${result_var})
                return()
            endif()

        endforeach(lib)

    endif()
endfunction( get_std_component_lib )

list(LENGTH AdditionalCxxStdLibs_FIND_COMPONENTS _ADDITIONAL_CXX_STD_LIBS_COMPONENTS_NUM )

if(_ADDITIONAL_CXX_STD_LIBS_COMPONENTS_NUM EQUAL 0)
    set(AdditionalCxxStdLibs_FIND_COMPONENTS ${_ADDITIONAL_CXX_STD_LIBS_COMPONENTS})
    foreach(component IN LISTS AdditionalCxxStdLibs_FIND_COMPONENTS)
        set(AdditionalCxxStdLibs_FIND_REQUIRED_${component} ${AdditionalCxxStdLibs_FIND_REQUIRED})
    endforeach(component)
endif()

set(_ADDITIONAL_CXX_STD_LIBS_LIBRARIES)

foreach(component IN LISTS AdditionalCxxStdLibs_FIND_COMPONENTS)
    if(NOT component IN_LIST _ADDITIONAL_CXX_STD_LIBS_COMPONENTS)
        if(NOT "${AdditionalCxxStdLibs_FIND_QUIETLY}")
            message(FATAL_ERROR "unknown component ${component} for AdditionalCxxStdLibs")
            continue()
        endif()
    endif()

    get_std_component_lib(
        "AdditionalCxxStdLibs_${component}_LIBRARY"
        "${_ADDITIONAL_CXX_STD_LIBS_HEADER_${component}}"
        "${_ADDITIONAL_CXX_STD_LIBS_VERSION_${component}}"
        "test_${component}.cpp"
        "${_ADDITIONAL_CXX_STD_LIBS_VARIANTS_${component}}"
        "${AdditionalCxxStdLibs_FIND_REQUIRED_${component}}"
        "${AdditionalCxxStdLibs_FIND_QUIETLY}"
    )

    if(DEFINED "AdditionalCxxStdLibs_${component}_LIBRARY")
        list(APPEND _ADDITIONAL_CXX_STD_LIBS_LIBRARIES "${AdditionalCxxStdLibs_${component}_LIBRARY}")
        set(AdditionalCxxStdLibs_${component}_FOUND true)
    endif()
endforeach(component)

foreach(component IN LISTS _ADDITIONAL_CXX_STD_LIBS_COMPONENTS)
    if(NOT component IN_LIST AdditionalCxxStdLibs_FIND_COMPONENTS)
        set(AdditionalCxxStdLibs_${component}_FOUND false)
    endif()
endforeach()

include(FindPackageHandleStandardArgs)

set(ALWAYS_VALID true)

# checks AdditionalCxxStdLibs_${component}_FOUND
find_package_handle_standard_args(AdditionalCxxStdLibs REQUIRED_VARS ALWAYS_VALID HANDLE_COMPONENTS )

if(AdditionalCxxStdLibs_FOUND)
    set(AdditionalCxxStdLibs_LIBRARIES ${_ADDITIONAL_CXX_STD_LIBS_LIBRARIES})
endif()

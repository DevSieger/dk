#include <atomic>

template<std::size_t Size_tp>
struct Data {
    unsigned char d_[Size_tp];
};

template<std::size_t Size>
void check(){
    Data<Size> initial;
    std::atomic<Data<Size>> atomic(initial);
    atomic.store(initial);
    atomic.compare_exchange_weak(initial, initial, std::memory_order_acquire);
    initial = atomic.load();
}

int main(int argc, char** argv){
    check<4>();
    check<8>();
    check<16>();
}

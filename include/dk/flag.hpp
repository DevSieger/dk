/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <type_traits>
#include <dk/template_util.hpp>

namespace dk {

template<typename Enum_tp>
class Flag{
public:
    static_assert (std::is_enum<Enum_tp>::value, "Enum_tp should be an enumeration type");

    using Enum  = Enum_tp;
    using Value = std::underlying_type_t<Enum_tp>;

    Flag() = default;

    // emulates Flag( Enum ... args)
    template<typename ... Args>
    constexpr Flag(std::enable_if_t<dk::variadic_and((std::is_same<Enum,Args>::value)...),Enum> arg,
         Args ... args):
        value_(static_cast<Value>(arg))
    {
        using aux = int[];
        (void) aux{(value_ |= static_cast<Value>(args),1)...};
    }

    friend constexpr Flag operator |  (const Flag& lhv, const Flag& rhv){
        return Flag( lhv.value_ | rhv.value_ );
    }
    friend constexpr Flag operator &  (const Flag& lhv, const Flag& rhv){
        return Flag( lhv.value_ & rhv.value_ );
    }
    friend constexpr Flag operator ^  (const Flag& lhv, const Flag& rhv){
        return Flag( lhv.value_ ^ rhv.value_ );
    }
    friend constexpr bool operator == (const Flag& lhv, const Flag& rhv){
        return lhv.value_ == rhv.value_ ;
    }
    friend constexpr bool operator != (const Flag& lhv, const Flag& rhv){
        return lhv.value_ != rhv.value_ ;
    }

    constexpr Flag operator ~ ()const{
        return Flag( ~value_);
    }
    constexpr Flag & operator |=  (const Flag& f){
        value_|=f.value_;
        return *this;
    }
    constexpr Flag &operator &=   (const Flag& f){
        value_&=f.value_;
        return *this;
    }
    constexpr Flag &operator ^=   (const Flag& f){
        value_^=f.value_;
        return *this;
    }

    constexpr explicit operator bool() const {
        return value_;
    }

private:
    Value value_{0};
    constexpr Flag(Value v):value_( v ){}
};

}

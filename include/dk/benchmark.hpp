/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <string>
#include <ostream>
#include <vector>
#include <dk/timer.hpp>
#include <functional>
#include <iomanip>
#include <cstdio>

namespace dk {

namespace detail {

class Basic_benchmark{
public:

    // todo: use duration
    /// nanoseconds
    using Duration = std::int64_t;

    using Size = std::uint64_t;

    void clear(){ records_.clear();}

    Basic_benchmark(Size samples=1000,Size tests=1, Duration upd_int=10000000):
        test_size_(tests),sample_size_(samples),update_interval_(upd_int)
    {}

    /// returns the number of tests for a single input instance
    Size sample_size() const { return sample_size_;}

    /// returns the number of input instances.
    Size test_size() const {return test_size_;}

    /// returns the minimal time interval between invocations of progress_callback
    Duration update_interval()const{
        return update_interval_;
    }

    Basic_benchmark & sample_size(Size a_sample_size){
        sample_size_=a_sample_size;
        return *this;
    }

    Basic_benchmark& test_size(Size s){
        test_size_=s;
        return *this;
    }

    Basic_benchmark & update_interval(Duration a_update_interval){
        update_interval_=a_update_interval;
        return *this;
    }

    // Validate_fun is bool(void)
    template<typename Test_fun,typename Pre_fun,typename Validate_fun>
    Duration test(const std::string& name, Test_fun test_fun, Validate_fun validate, Pre_fun pre_fun=nullptr){
        using Timer_type = dk::Basic_timer<std::chrono::duration<Duration,std::nano>>;
        Duration total=0;
        Size n_failed=0;
        Timer_type timer;
        Timer_type status_timer;
        Duration since_last_upd = 0;

         auto upd_status = [&since_last_upd,&status_timer,&n_failed,this](Size set,Size sample){
             if(!progress_callback) return;
            since_last_upd += status_timer.restart();
            if(since_last_upd>=update_interval()){
                since_last_upd=0;
                progress_callback( set,sample,n_failed);
            }
        };

        if(progress_callback) progress_callback(0,0,0);

        for(Size i=0;i<test_size_;++i){
            prepare(pre_fun);
            upd_status(i,0);
            test_fun();
            if(validate()){
                Duration minimal = std::numeric_limits<Duration>::max();
                for(Size s=0;s<sample_size_;++s){
                    timer.start();
                    test_fun();
                    Duration dur = timer.elapsed();
                    minimal = std::min( minimal, dur);
                    upd_status(i,s+1);
                }
                total+=minimal;
            }else{
                ++n_failed;
            }
        }
        if(test_size_>n_failed){
            total/=test_size_-n_failed;
        };
        records_.emplace_back(name,total,n_failed);
        return total;
    }

    void show(std::ostream& out)const{
        if(records_.empty()) return;

        using Size = std::string::size_type;
        Size max_name_len = 0;
        Size max_dur_len = 0;

        std::vector<std::string> durations;
        durations.reserve(records_.size());


        for(auto&& entry: records_){
            Size len = std::get<0>(entry).size();
            if(max_name_len<len) max_name_len = len;
            durations.emplace_back( std::to_string(to_ms(std::get<1>(entry))));
            len = durations.back().size();
            if(max_dur_len < len) max_dur_len = len;
        }


        for(std::size_t ix=0; ix<records_.size();++ix){
            auto& e = records_[ix];
            out<<std::setw(int(max_name_len))<<std::left<<std::get<0>(e)
              <<" : "
             <<std::setw(int(max_dur_len))<<std::right<<durations[ix]<<" ms";
            if(std::get<2>(e)){
                out<<", failed: "<<std::get<2>(e);
            }
            out<<std::endl;
        }
    }

    /*
     * the first parameter of callback is the number of completed data sets,
     * the second parameter is the number of samples completed for the data set running,
     * the third one is the number of data sets that have produced incorrect output.
     */
    std::function< void(Size,Size,Size)> progress_callback;

    double to_ms(Duration d)const{
        return 0.000001*d;
    }
private:

    template<typename Pre_fun>
    void prepare(Pre_fun pre_fun)const{ pre_fun();}

    void prepare(std::nullptr_t)const{}

    Size test_size_;
    Size sample_size_;
    Duration update_interval_;
    std::vector<std::tuple<std::string,Duration,Size>> records_;
};

inline std::ostream& operator << (std::ostream& out,Basic_benchmark bm){
    bm.show(out);
    return out;
}

class Progress_printer{
public:
    using Size = Basic_benchmark::Size;
    using String = std::string;
    Progress_printer(std::ostream& ostr, const Basic_benchmark& bmrk):
        ostr_(ostr),bmrk_(bmrk)
    {}

    void finish(){
        stream()<<erase_str_+String(erase_str_.size(),' ')+erase_str_;
    }
    void start(const String & prefix = String{}){
        init(prefix);
        stream()<<String(erase_str_.size(),' ');
    }

    void operator() (Size test,Size sample, Size n_failed){
        update(test,sample,n_failed);
        stream()<< cache_;
    }

    std::ostream& stream()const{ return ostr_;}

private:
    struct Field{
        std::size_t begin;
        std::size_t size;
        char replacement;
    };
    struct{
        Field total;
        Field set;
        Field sample;
        Field errors;
    } fields_;

    String cache_;
    String erase_str_;
    std::ostream& ostr_;
    const Basic_benchmark& bmrk_;

    inline void init(const String& prefix);

    void update(Size test,Size sample, Size n_failed){
        Size  smpl_n = bmrk_.sample_size(), set_n = bmrk_.test_size();
        double prc = 100.0*(double(test)*smpl_n+sample)/smpl_n/set_n;

        upd_field(fields_.total,prc);
        upd_field(fields_.set,test);
        upd_field(fields_.sample,sample);
        upd_field(fields_.errors,n_failed);
    }

    void upd_field(const Field& f,Size v){
        std::snprintf(&cache_[f.begin], f.size, "%*llu",f.size-1,static_cast<unsigned long long>(v));
        cache_[f.begin+f.size-1]=f.replacement;
    }

    void upd_field(const Field& f,double v){
        std::snprintf(&cache_[f.begin], f.size, "%*.2f",f.size-1,v);
        cache_[f.begin+f.size-1]=f.replacement;
    }
};


inline void Progress_printer::init(const String & prefix){
    String max_set = std::to_string(bmrk_.test_size());
    String max_sample = std::to_string(bmrk_.sample_size());
    String str;
    if(!prefix.empty()){
        str+=prefix+": ";
    };
    str+="[";

    auto make_field = [&str](auto&& val,  Field& f){
        f.begin=str.size();
        str+=std::forward<decltype(val)>(val);
        f.size=str.size()-f.begin+1; //we need +1 for \0
    };

    make_field("100.00",fields_.total);
    str+="%]  ";
    make_field(max_set,fields_.set);
    str+=" / "+max_set+" : ";
    make_field(max_sample,fields_.sample);
    str+=" / "+max_sample+", errors: ";
    make_field(max_set,fields_.errors);
    str+='\0';

    cache_=str;
    erase_str_ = String(str.size(),'\b');
    cache_ = erase_str_+cache_;

    for( Field* f: { &fields_.total, &fields_.set, &fields_.sample, &fields_.errors }){
        f->begin+=erase_str_.size();
        f->replacement = cache_[f->begin+f->size-1];
    }
}

}

class Benchmark: public detail::Basic_benchmark{
    using Base = detail::Basic_benchmark;
    using Progress_printer = detail::Progress_printer;
public:
    Benchmark(std::ostream& ostr, Size samples=1000,Size tests=1, Duration upd_int=10000000):
        Base(samples,tests,upd_int),printer_(ostr,*this)
    {
        progress_callback=std::reference_wrapper<Progress_printer>(printer_);
    }

    bool use_prefix()const{
        return use_prefix_;
    }
    void use_prefix(bool p_use_prefix_){
        use_prefix_=p_use_prefix_;
    }

    // Validate_fun is bool(void)
    template<typename Test_fun,typename Pre_fun,typename Validate_fun>
    Duration test(const std::string& name, Test_fun test_fun, Validate_fun validate, Pre_fun pre_fun=nullptr){
        using std::endl;
        std::string prefix;
        if(use_prefix()) prefix=name;
        printer_.start(prefix);
        Duration dur = Base::test(name,test_fun,validate,pre_fun);
        printer_.finish();
        return dur;
    }

    void print_results()const{
        show(printer_.stream());
    }

private:
    using Base::progress_callback;
    using Base::show;
    Progress_printer printer_;
    bool use_prefix_=true;
};

}

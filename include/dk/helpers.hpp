/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <dk/template_util.hpp>
#include <dk/parameter_pack.hpp>
#include <tuple>
#include <array>

namespace dk {

namespace dtl {
namespace helpers{

namespace apply_visitor {

// to get a pointer to the function that calls f with Ix'th element of the tuple-like arg
template<typename R, typename F, typename T, std::size_t Ix>
constexpr R apply_fn(F f, T arg){
    return std::forward<F>(f) (std::get<Ix>(std::forward<T>(arg)));
}
template<typename R, typename F, typename T, std::size_t ... Ix>
constexpr std::array<R(*)(F,T), sizeof...(Ix)> make_array(std::index_sequence<Ix...>){
    return {&apply_fn<R,F,T,Ix>...};
}

} // namespace apply_visitor

template<typename Pck, typename Ix_seq>
struct EBO_pack_helper;

template<typename ... Tps, std::size_t ... Ix>
struct EBO_pack_helper<dk::Pack<Tps...>, std::index_sequence<Ix...>>: dk::EBO_wrapper<Tps,Ix>...{

    static_assert ( sizeof... (Tps)==sizeof... (Ix), "pack sizes missmatch" );

    /// the type of I-th object
    template<std::size_t I>
    using Base = dk::EBO_wrapper<dk::type_at_t<I, Tps...>, I>;

    using Types = dk::Pack<Tps...>;

    template<typename ... U>
    EBO_pack_helper(U&& ... args): Base<Ix>(std::forward<U>(args))... {}

    /// returns reference to I-th object (from Tps)
    template<std::size_t I>
    decltype (auto) get()       &   noexcept{ return Base<I>::get(); }

    template<std::size_t I>
    decltype (auto) get() const &   noexcept{ return Base<I>::get(); }

    template<std::size_t I>
    decltype (auto) get()       &&  noexcept{ return std::move(*this).Base<I>::get(); }

};

} // namespace helpers
} // namespace dtl

/// calls the given functor f with i-th element of the given tuple t
template <typename R, typename F, typename ... Args>
constexpr R apply_tuple_visitor(std::size_t i, F&& f, std::tuple<Args...>& t){
    using Tuple         = decltype (t);
    namespace ns        = dtl::helpers::apply_visitor;
    constexpr auto arr  = ns::make_array<R,F&&,Tuple>(std::index_sequence_for<Args...>{});
    return arr[i](std::forward<F>(f),t);
}
template <typename R, typename F, typename ... Args>
constexpr R apply_tuple_visitor(std::size_t i, F&& f, std::tuple<Args...>&& t){
    using Tuple         = decltype (t);
    namespace ns        = dtl::helpers::apply_visitor;
    constexpr auto arr  = ns::make_array<R,F&&,Tuple>(std::index_sequence_for<Args...>{});
    return arr[i](std::forward<F>(f),std::forward<Tuple>(t));
}

/// calls the given functor f with i-th argument from the passed arguments args
template <typename R, typename F, typename ... Args>
constexpr R apply_parameter_visitor(std::size_t i, F&& f, Args&&... args){
    return apply_tuple_visitor<R>(i,std::forward<F>(f), std::forward_as_tuple(std::forward<Args>(args)...));
}

/*!
 * \brief Helper class to use EBO(empty base optimization) with the given type parameter pack Tps
 *
 * rationale: automates indexation, provides simple access to the packed types.
 * use EBO_pack::get<N>() to acceess N-th type in Tps.
 *
 * hint: don't forget template keyword when it is required:
 * \code EBO_pack<Tps...>::template get<N>() \endcode
 */
template <typename ... Tps>
using EBO_pack = dtl::helpers::EBO_pack_helper< dk::Pack<Tps...>,std::index_sequence_for<Tps...>>;

/// Helpers for allocator template parameter
namespace alloc {

namespace dtl {

template <typename Lhv, typename Rhv>
inline void propagate_on_assign_hlpr(Lhv& lhv, Rhv&& rhv, std::true_type){
    lhv = std::forward<Rhv>(rhv);
}
template <typename Lhv, typename Rhv>
inline void propagate_on_assign_hlpr(Lhv& lhv, Rhv&& rhv, std::false_type){
    (void)lhv;(void)rhv;
}

template <typename Alloc>
inline void propagate_on_swap_hlpr(Alloc& lhv, Alloc& rhv, std::true_type){
    using std::swap;
    swap(lhv,rhv);
}
template <typename Alloc>
inline void propagate_on_swap_hlpr(Alloc& lhv, Alloc& rhv, std::false_type){
    (void)lhv;(void)rhv;
}

// void_t emulation, no C++17
template <typename ... Tps>
struct Void_t{
    using type=void;
};

/*
 * there was no allocator_traits::is_always_equal before c++17,
 * but some implementations (look at gcc) uses it even if __cplusplus <= 201402,
 * so I think it is safe to emulate it.
 */
template<typename Alloc_tp,typename T  = void>
struct Alloc_is_always_equal_emulation: std::is_empty<Alloc_tp>::type {};

// ignores if is_always_equal is non-type member, it is possible to handle, but std::allocator_traits does the same.
template<typename Alloc_tp>
struct Alloc_is_always_equal_emulation<Alloc_tp, typename Void_t<typename Alloc_tp::is_always_equal>::type>:
        Alloc_tp::is_always_equal
{
    static_assert(
            std::is_base_of<std::true_type,typename Alloc_tp::is_always_equal>::value ||
            std::is_base_of<std::false_type,typename Alloc_tp::is_always_equal>::value,
            "Alloc_tp::is_always_equal should be derived from or equal to std::true_type or std::false_type");
};

template<typename Alloc_tp,typename T  = void>
struct Alloc_is_always_equal: Alloc_is_always_equal_emulation<Alloc_tp>{};

template<typename Alloc_tp>
struct Alloc_is_always_equal<Alloc_tp, typename Void_t< typename std::allocator_traits<Alloc_tp>::is_always_equal>::type>:
        std::allocator_traits<Alloc_tp>::is_always_equal {
};

// is_nothrow_swappable emulation, if no C++17
#if __cplusplus > 201402ul
template <typename T>
using Is_nothrow_swappable = std::is_nothrow_swappable<T>;
#else

namespace Is_nothrow_swappable_ns{
using std::swap;

template <typename T,typename = std::true_type>
struct Hlpr: std::false_type{};

template <typename T>
struct Hlpr<T,std::integral_constant<bool,noexcept (swap(std::declval<T&>(),std::declval<T&>()))>>: std::true_type{};

}

template <typename T>
using Is_nothrow_swappable = Is_nothrow_swappable_ns::Hlpr<T>;
#endif


} // namespace dtl

template <typename Alloc>
inline void propagate_on_assign(Alloc& lhv, const Alloc& rhv){
    using Propagate = typename std::allocator_traits<Alloc>::propagate_on_container_copy_assignment;
    dtl::propagate_on_assign_hlpr(lhv,rhv, Propagate{});
}

template <typename Alloc>
inline void propagate_on_assign(Alloc& lhv, Alloc&& rhv){
    using Propagate = typename std::allocator_traits<Alloc>::propagate_on_container_move_assignment;
    dtl::propagate_on_assign_hlpr(lhv,std::move(rhv), Propagate{});
}

template <typename Alloc>
inline void propagate_on_swap(Alloc& lhv, Alloc& rhv){
    using Propagate = typename std::allocator_traits<Alloc>::propagate_on_container_swap;
    dtl::propagate_on_swap_hlpr(lhv,rhv, Propagate{});
}

template< typename Alloc>
using is_always_equal = dtl::Alloc_is_always_equal<Alloc>;

template <typename Alloc>
using allows_nothrow_move_assignment =
    std::integral_constant<bool, is_always_equal<Alloc>::value ||
        std::allocator_traits<Alloc>::propagate_on_container_move_assignment::value >;

template <typename Alloc>
using allows_nothrow_swap =
    std::integral_constant<bool,
        ( ! std::allocator_traits<Alloc>::propagate_on_container_swap::value && is_always_equal<Alloc>::value) ||
        (   std::allocator_traits<Alloc>::propagate_on_container_swap::value && dtl::Is_nothrow_swappable<Alloc>::value) >;

} // namespace alloc

} // namespace dk

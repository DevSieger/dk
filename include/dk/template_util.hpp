/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef TEMPLATE_UTIL_H
#define TEMPLATE_UTIL_H
#include <initializer_list>
#include <type_traits>
#include <utility>

namespace dk {
    inline void variadic_pass(std::initializer_list<int>){}

    constexpr bool variadic_and(){
        return true;
    }
    template<typename ... Args>
    constexpr bool variadic_and(bool v,Args ... vals){
        return v && variadic_and(vals...);
    }


    /**
     * \brief Type to perform the empty base optimization if possible
     *
     * Usage: inherit from it to build packed class.
     * hint: for the best result make packed the second data member, not the entire class.
     *
     * we need ID to enable inheritance from the same T many times:
     * for example: "class Test: EBO_helper<T,0>,EBO_helper<This_is_T_alias,1>"
     */
    template<
            typename T, unsigned ID, bool ebo = std::is_empty<T>::value
    #if __cplusplus >= 201402L
             && !std::is_final<T>::value
    #endif
    >
    class EBO_wrapper;

    template<typename T,unsigned ID>
    class EBO_wrapper<T,ID,true>: T{
    public:
        using Value = T;

        EBO_wrapper() = default;
        EBO_wrapper(const Value& other) noexcept(std::is_nothrow_copy_constructible<Value>::value):
            Value(other){}
        EBO_wrapper(Value&& other)      noexcept(std::is_nothrow_move_constructible<Value>::value):
            Value(std::move(other)){}

        Value&          get() &         noexcept    { return *this; }
        const Value&    get() const &   noexcept    { return *this; }
        Value&&         get() &&        noexcept    { return std::move(*this); }
    };

    template<typename T,unsigned ID>
    class EBO_wrapper<T,ID,false>{
    public:
        using Value = T;

        EBO_wrapper() = default;
        EBO_wrapper(const Value& other) noexcept(std::is_nothrow_copy_constructible<Value>::value):
            m_(other){}
        EBO_wrapper(Value&& other)      noexcept(std::is_nothrow_move_constructible<Value>::value):
            m_(std::move(other)){}

        Value&          get() &         noexcept    { return m_; }
        const Value&    get() const &   noexcept    { return m_; }
        Value&&         get() &&        noexcept    { return std::move(m_); }

    private:
        Value m_;
    };
}
#endif // TEMPLATE_UTIL_H

/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <dk/parameter_pack.hpp>

namespace dk {

constexpr std::size_t adapt_max_params = 8;

/// converts template<typename ...> class F to template with the given number of parameters (up to adapt_max_params)
template < template <typename ... Args>  class F, std::size_t n_params>
struct Adapt;

/// placeholder type refering to template parameter Ix of Bind::result
template<std::size_t Ix>
struct Param : std::integral_constant<std::size_t, Ix>{};

/// std::bind for metaprogramming
template< template <typename ... Args>  class F, typename ... Tps>
class Bind{
    template<typename T, typename ... Ps>
    struct P_dispatch{
        using type = T;
    };

    template<std::size_t Ix, typename ... Ps >
    struct P_dispatch<Param<Ix>,Ps...>{
        using type = type_at_t<Ix,Ps...>;
    };

    template<template <typename ... As>  class F2,typename ... Args, typename ... Ps >
    struct P_dispatch<Bind<F2,Args...>, Ps...>{
        using type = typename Bind<F2,Args...>::template result<Ps...>;
    };
public:
    template<typename ... Ps>
    using result = F< typename P_dispatch<Tps,Ps...>::type ... >;
};

/// the same as Bind, but produces template that has N parameters.
template< std::size_t N, template <typename ... Args>  class F, typename ... Tps>
using Bind_n = Adapt< Bind<F, Tps...>::template result, N>;


//-------------------------------------------------//
// I don't know a better way to write that :)
template < template <typename ... Args>  class F>
struct Adapt<F,0>{
    using result = F<>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,1>{
    template<typename T0> using result = F<T0>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,2>{
    template<typename T0,typename T1>
    using result = F<T0,T1>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,3>{
    template<typename T0,typename T1,typename T2>
    using result = F<T0,T1,T2>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,4>{
    template<typename T0,typename T1,typename T2,typename T3>
    using result = F<T0,T1,T2,T3>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,5>{
    template<typename T0,typename T1,typename T2,typename T3,typename T4>
    using result = F<T0,T1,T2,T3,T4>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,6>{
    template<typename T0,typename T1,typename T2,typename T3,typename T4,typename T5>
    using result = F<T0,T1,T2,T3,T4,T5>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,7>{
    template<typename T0,typename T1,typename T2,typename T3,typename T4,typename T5,typename T6>
    using result = F<T0,T1,T2,T3,T4,T5,T6>;
};
template < template <typename ... Args>  class F>
struct Adapt<F,8>{
    template<typename T0,typename T1,typename T2,typename T3,typename T4,typename T5,typename T6,typename T7>
    using result = F<T0,T1,T2,T3,T4,T5,T6,T7>;
};

}

/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <chrono>
#include <utility>

namespace dk {

namespace detail {

template<typename Duration>
struct is_duration: std::false_type {};

template<typename Rep, typename Ratio>
struct is_duration<std::chrono::duration<Rep,Ratio>>: std::true_type {};

}

template<typename Duration_t>
class Basic_timer
{
    static_assert( detail::is_duration<Duration_t>::value,"Duration_t should be std::chrono::duration");
public:
    using Duration    = Duration_t;
    using Time_period = typename Duration::rep;

    /*!
     * \brief Starts the timer.
     * \param offset    Initial time in the timer, in seconds.
     */
    void start(Time_period offset = 0){
        last_ = Clock::now();
        if( offset ) set_offset(offset);
    }

    /*!
     * \brief Restarts the timer.
     * \param offset    Initial time in the timer, in seconds.
     * \return Time elapsed since the start of the timer.
     */
    Time_period restart(Time_period offset = 0){
        TimePoint last  = last_;
        last_           = Clock::now();
        Time_period ret = std::chrono::duration_cast<Duration>( last_ - last ).count();
        if( offset ) set_offset( offset);
        return ret;
    }

    /// Returns time elapsed since the start of the timer.
    Time_period elapsed()const{
        return std::chrono::duration_cast<Duration>(Clock::now()-last_).count();
    }

private:
    using Clock             = std::chrono::steady_clock;
    using TimePoint         = Clock::time_point;

    TimePoint last_;

    void set_offset( Time_period offset){
        last_ -= std::chrono::duration_cast<Clock::duration>( Duration(offset) );
    }
};

using Timer = Basic_timer<std::chrono::duration<double,std::ratio<1,1>>>;

}

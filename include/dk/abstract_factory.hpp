/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <memory>
#include <dk/parameter_pack.hpp>

namespace dk {


template<typename ... Bs>
class Abstract_factory;

template<typename T>
struct Construction_traits{
    using Pointer = std::unique_ptr<T>;

    template<typename Factory>
    static Pointer construct(Factory& base){
        (void)base;
        return std::make_unique<T>();
    }
};

namespace detail {

template<typename>
struct Type_tag{};

template<typename ... Tps>
struct Factory_ifc;


template<typename T>
struct Factory_ifc<T>{
    virtual typename Construction_traits<T>::Pointer do_create(Type_tag<T>) = 0;
    virtual ~Factory_ifc() = default;
};

// this was writen before dk::fold_t, no need to rewrite
template<typename T, typename ... Tps>
struct Factory_ifc<T,Tps...>:  Factory_ifc<Tps...>{
    using Base =  Factory_ifc<Tps...>;
    using Base::do_create;
    virtual typename Construction_traits<T>::Pointer do_create(Type_tag<T>) = 0;
};



template<typename Tp>
struct is_abstract_factory: std::false_type{};

template<typename ... Tps>
struct is_abstract_factory<Abstract_factory<Tps...>>: std::true_type{};


template<typename Ifc, typename Base_pack, typename Derived_pack>
class Factory_impl;

template<typename Ifc>
class Factory_impl<Ifc, Pack<> ,Pack<> >: public Ifc{};

template<typename Ifc, typename B, typename D, typename ... Bs, typename ... Ds>
class Factory_impl<Ifc, Pack<B,Bs...>, Pack<D,Ds...>>: public Factory_impl< Ifc, Pack<Bs...>,Pack<Ds...> >{
    virtual typename Construction_traits<B>::Pointer do_create(Type_tag<B>) override{
        return Construction_traits<D>::construct(*this);
    }
};

}

template<typename ... Bs>
class Abstract_factory: detail::Factory_ifc<Bs...>{
    using Base = detail::Factory_ifc<Bs...>;
    using Base::do_create;
public:

    template<typename B>
    typename Construction_traits<B>::Pointer create(){
        return do_create(detail::Type_tag<B>{});
    }
};

template<typename Base_tp, typename ... Ts>
class Concrete_factory{
    static_assert(detail::is_abstract_factory<Base_tp>::value, "Base_tp have to be an instantion of Abstract_factory");
};

template<typename ... Bs, typename ... Ds>
class Concrete_factory< Abstract_factory<Bs...>, Ds...>:
        public detail::Factory_impl<Abstract_factory<Bs...>,
        Pack<Bs...>, Pack<Ds...>>{
};

}

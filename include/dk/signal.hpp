/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <functional>
#include <list>
#include <forward_list>
#include <memory>
#include <deque>
#include <type_traits>
#include <utility>
#include <exception>

namespace dk {

namespace collect{

/// a collecting object that ignores any results
template <typename Collected_tp>
class Collect_ignore;

}

/*!
 * \brief a part of Signal providing callback modification methods
 *
 * this one is detached to simplify exposition of the callback management interface
 * for users of the class that contains Signal and hides the emission interface.
 */
template <typename Callback_tp>
class Callbacks_base;

template < typename Call_res_tp, typename... Tps>
class Callbacks_base< Call_res_tp(Tps...) >{
    using List      = std::list<std::shared_ptr<std::function<Call_res_tp(Tps...)>>>;
public:

    /// The type of a callback
    using Functor   = std::function<Call_res_tp(Tps...)>;

    /// The handle associated with a callback
    using Handle    = typename List::const_iterator;

    /// adds a callback constructed from the given arguments
    template<typename ... Args>
    std::enable_if_t<std::is_constructible<Functor,Args...>::value,Handle>
    add( Args&& ... args){
        auto sp = std::make_shared<Functor>(std::forward<Args>(args)...);
        return list_.insert(list_.end(),std::move(sp));
    }

    /// removes the callback associated with the given handle from emissions, exluding incomplete ones
    void remove(Handle h){
        list_.erase(h);
    }

    /// removes the callback associated with the given handle from emissions, including incomplete ones
    void hard_remove(Handle h){
        if(*h) **h = nullptr;
        remove(h);
    }

    /// removes all callbacks, doesn't effect incomplete emissions
    void clear(){
        list_.clear();
    }

    /// removes all callbacks, effects incomplete emissions
    void hard_clear(){
        for(auto&& cb: list_){
            if(cb) *cb = nullptr;
        }
        clear();
    }

    /// returns the number of callbacks to invoke on emission, exluding incomplete ones
    std::size_t size() const {
        return list_.size();
    }

    /// returns true if there is no callbacks to invoke on emission
    bool empty() const {
        return list_.empty();
    }

    static Handle invalid_handle() {
        // singular
        return Handle{};
    }

    bool emission_running(){
        return !emission_list_.empty();
    }

protected:
    ~Callbacks_base()                   = default;
    Callbacks_base()                    = default;

    /*
     * The presence of a move constructor introduce troubles for a
     * recursive emission. The solution that supports both a move
     * constructor and a recursive emission is over-complicated.
     * IMO it is optimal to disable a move constructor. Use smart
     * pointers if you need moving.
    */
    Callbacks_base(Callbacks_base&&)    = delete;

    std::list<std::shared_ptr<Functor>>
            list_;

    // we need to iterate to insert elements to a "back" of the list
    // but this shouldn't change a complexity
    // void* here is a hack, we don't know a type of the collector
    std::forward_list<void*>
            emission_list_;
};

/*!
 * \brief Simple, thread unsafe, non-optimazed signal that supports recursive emissions.
 * \tparam Callback_tp      the callback return type and parameter types, in the form of function type ( i.e. R(P1,P2,P3,...) )
 * \tparam Collect_obj_tp   a type of an object that collects callback results
 *
 * see collect::Collect_ignore for details about collecting objects.
 * callback parameters should be copy-constructible.
 * use rvalue-references for callback parameters with care.
 */
template <typename Callback_tp, template<typename> class Collect_obj_tp = collect::Collect_ignore>
class Signal;

template < typename Call_res_tp,template<typename> class Collect_obj_tp, typename... Tps>
class Signal< Call_res_tp(Tps...), Collect_obj_tp >: public Callbacks_base<Call_res_tp(Tps...)>{
public:
    using Callbacks = Callbacks_base<Call_res_tp(Tps...)>;
    using typename Callbacks::Functor;

    /// The type of object to combine callback invocation results
    using Collect   = Collect_obj_tp<Call_res_tp>;

    // to reduce copying, we can't use move, we have to copy because of multiple callbacks.
    template<typename T>
    using Param     = std::conditional_t< std::is_class<T>::value || std::is_union<T>::value, const T&, T>;

    /// A type of an emission result
    using Result    = typename Collect::Result;

    /// the same as emit
    Result operator()(Param<Tps>... args) {
        return emit(std::forward<Param<Tps>>(args)...);
    }

    /// emits the signal calling all _current_ callbacks
    Result emit(Param<Tps>... args) {
        auto& cb_list   = Callbacks::list_;

        Emission_instance instance {
            {}, {args...}, {cb_list.begin(), cb_list.end()}
        };

        register_emission(instance);
        Emission_instance* active_emission;
        while( (active_emission = next_emission()) != nullptr ){
            process_emission(*active_emission);
        }
        return instance.collector.get();
    }

private:
    template<typename T> struct Tag{};

    template < typename T, typename = bool >
    struct Exc_tag: std::false_type {};

    template < typename T>
    struct Exc_tag
            < T, decltype ( static_cast<bool>(
                                std::declval<T&>().handle(std::current_exception())))>:
        std::true_type {};

    using Args      = std::tuple<Param<Tps>...>;
    using Fn_queue  = std::deque<std::shared_ptr<Functor>>;

    struct Emission_instance {
        Collect     collector;
        Args        args;
        Fn_queue    fn_queue;
    };

    void register_emission(Emission_instance& inst){
        auto& elist = Callbacks::emission_list_;
        auto it     = elist.before_begin();
        auto last   = it;

        while( ++it != elist.end()) {
            last = it;
        }

        elist.insert_after(last, &inst);
    }

    Emission_instance* next_emission(){
        auto& elist = Callbacks::emission_list_;

        auto get_front  =   [&elist] () {
            return static_cast<Emission_instance*>(elist.front());
        };

        while (!elist.empty() && get_front()->fn_queue.empty()) {
                elist.pop_front();
        }
        return elist.empty() ? nullptr : get_front();
    }

    void process_emission(Emission_instance& inst) {
        auto& queue = inst.fn_queue;

        while( !queue.empty() ){
            std::shared_ptr<Functor> fptr   = std::move( queue.front() );
            queue.pop_front();
            if( ! *fptr ) continue;
            if(!invoke_and_catch(Exc_tag<Collect>{}, *fptr, inst.collector, inst.args ) )
                break;
        }

        // if an invocation loop has been broken
        if(!queue.empty()) queue.clear();
    }

    static bool invoke_and_catch (std::false_type, const Functor& f, Collect& co, Args& args) {
        return invoke(Tag<Call_res_tp>(), std::index_sequence_for<Tps...>{}, f, co, args );
    }
    static bool invoke_and_catch (std::true_type, const Functor& f, Collect& co, Args& args) {
        try {
            return invoke(Tag<Call_res_tp>(), std::index_sequence_for<Tps...>{}, f, co, args );
        } catch (...) {
            return co.handle(std::current_exception());
        }
    }

    template<typename T, std::size_t ... Ix>
    static bool invoke( Tag<T>, std::index_sequence<Ix...>,
                        const Functor& f, Collect& co, Args& args ){
        using std::get;
        return co.collect( f( std::get<Ix>(args)... ) );
    }

    template<std::size_t ... Ix>
    static bool invoke( Tag<void>, std::index_sequence<Ix...>,
                        const Functor& f, Collect& co, Args& args ){
        f( std::get<Ix>(args)... );
        return co.collect();
    }
};


namespace collect{

/// Sample class to describe what the collector object have to implement and how
template <typename Value_tp>
class Collector_example {
public:

    /// A default constructor
    Collector_example() = default;

    /// a type of a collection result, may be void
    using Result    = void;

    /*!
     * \brief Collects the result of a callback invocation
     * \param   rvalue_arg    the result of a callback invocation
     * \return  false to indicate that the current signal emission should stop,
     *          true otherwise
     *
     * \warning In the presence of an recursive emission:
     * - an invocation order of the method doesn't match the invocation order
     *   of the callbacks
     * - the method may be called again even if it ( or #handle ) has returned
     *   false before.
    */
    bool collect(Value_tp&& rvalue_arg){
        (void) rvalue_arg;
        return true;
    }

    /*!
     * \brief Handles an exception
     * \return  false to indicate that the current signal emission should stop,
     *          true otherwise
     *
     * The exception to handle may be thrown from an invoked callback,
     * a move constructor of Value_tp or a call to #collect.
     *
     * The presence of this method is optional. If it doesn't present than
     * Signal doesn't handle exceptions at all, and Signal::emit may throw
     * an exception from the running "parent" emission.
     *
     * \warning In the presence of an recursive emission:
     * - an invocation order of the method doesn't match the invocation order
     *   of the callbacks
     * - the method may be called again even if it ( or #collect ) has returned
     *   false before.
     */
    bool handle(std::exception_ptr){
        return true;
    }

    /*!
     * \brief Returns the collected value, throws the stored exception(s).
     *
     * The returned value serves as the result of the current signal
     * emission.
     */
    Result get(){}
};

/// a collecting object that ignores any results
template <typename Collected_tp>
class Collect_ignore{
public:
    /// a type of a collection result
    using Result    = void;

    /// a type of callback invocation results
    using Value     = Collected_tp;

    /*!
     * \brief collects the result of a callback invocation
     * \param the result of a callback invocation
     * \return false to indicate that the current signal emission should stop, true otherwise
     *
     * void results is handled using Collect_ignore<void> specialization
     */
    bool collect(Value&& args){
        (void) args;
        return true;
    }

    /// returns the collected value.
    ///
    /// the returned value serves as the result of the current signal emission.
    Result get(){}
};

// void is the special case
template <>
class Collect_ignore<void>{
public:
    using Result    = void;
    using Value     = void;

    bool collect(void){
        return true;
    }
    Result get(){}
};

} // namespace collect
} // namespace dk

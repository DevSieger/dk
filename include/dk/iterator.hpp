/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <iterator>
#include <utility>
#include <memory>
#include <dk/template_util.hpp>

namespace dk {

/// Iterator adaptor that passes the result of an inderection of the original iterator through the given functor
template <typename Iterator, typename Fn_tp>
class Fn_iterator: dk::EBO_wrapper<Fn_tp,0>{
    using Itrt = std::iterator_traits<Iterator>;
    using Dereference_t     = decltype (*std::declval<const Iterator&>());
public:
    using Base              = Iterator;
    using Functor           = Fn_tp;

    using difference_type   = typename Itrt::difference_type;
    using iterator_category = typename Itrt::iterator_category;

    // may be the proxy object
    using reference         = decltype(std::declval<Fn_tp>()(std::declval<Dereference_t>()));
    using value_type        = std::decay_t<reference>;
    using pointer           = value_type*;


    constexpr Fn_iterator(): inner_() {}
    constexpr Fn_iterator(Functor f): Fn_base(std::move(f)), inner_() {}
    constexpr explicit Fn_iterator(Iterator it): inner_(it) {}
    constexpr explicit Fn_iterator(Iterator it, Functor f): Fn_base(std::move(f)), inner_(it) {}

    template<typename U>
    constexpr Fn_iterator(
            std::enable_if_t<std::is_convertible<U,Iterator>::value, const Fn_iterator<U,Functor>&> other
            ):
        Fn_base(other.fn()), inner_(other.base())
    {}

    template<typename U>
    constexpr Fn_iterator& operator = (
            std::enable_if_t<std::is_convertible<U,Iterator>::value, const Fn_iterator<U,Functor>&> other
            )
    {
        inner_  = other.base();
        fn()    = other.fn();
    }

    constexpr const Iterator& base() const { return inner_; }

    constexpr decltype(auto) operator*() const {
        return fn()(*inner_);
    }

    constexpr pointer operator->() const {
        static_assert (std::is_lvalue_reference<Dereference_t>::value, "invalid return type in operator* of Base iterator" );
        return std::addressof(fn()(*inner_));
    }

    constexpr decltype(auto) operator[] (difference_type n) const{
        return fn()(inner_[n]);
    }

    constexpr Fn_iterator& operator++(){
        ++inner_;
        return *this;
    }
    constexpr Fn_iterator& operator++(int){
        return Fn_iterator(inner_++);
    }

    constexpr Fn_iterator& operator--(){
        --inner_;
        return *this;
    }
    constexpr Fn_iterator& operator--(int){
        return Fn_iterator(inner_--);
    }

    constexpr Fn_iterator operator+(difference_type n) const{
        return Fn_iterator(inner_+n);
    }
    constexpr Fn_iterator operator-(difference_type n) const{
        return Fn_iterator(inner_-n);
    }

    constexpr Fn_iterator& operator+=(difference_type n){
        inner_+=n;
        return *this;
    }
    constexpr Fn_iterator& operator-=(difference_type n){
        inner_-=n;
        return *this;
    }

    constexpr       Functor& fn() {
        return Fn_base::get();
    }
    constexpr const Functor& fn() const{
        return Fn_base::get();
    }

    friend constexpr Fn_iterator operator+(difference_type n, const Fn_iterator& it){
        return it+n;
    }

private:
    using Fn_base = dk::EBO_wrapper<Functor,0>;

    Iterator inner_;
};

template<typename Lhi,typename Rhi, typename Fn>
constexpr bool operator == (const Fn_iterator<Lhi,Fn>& li, const Fn_iterator<Rhi,Fn>& ri){
    return li.base() == ri.base();
}
template<typename Lhi,typename Rhi, typename Fn>
constexpr bool operator != (const Fn_iterator<Lhi,Fn>& li, const Fn_iterator<Rhi,Fn>& ri){
    return li.base() != ri.base();
}
template<typename Lhi,typename Rhi, typename Fn>
constexpr bool operator < (const Fn_iterator<Lhi,Fn>& li, const Fn_iterator<Rhi,Fn>& ri){
    return li.base() < ri.base();
}
template<typename Lhi,typename Rhi, typename Fn>
constexpr bool operator <= (const Fn_iterator<Lhi,Fn>& li, const Fn_iterator<Rhi,Fn>& ri){
    return li.base() <= ri.base();
}
template<typename Lhi,typename Rhi, typename Fn>
constexpr bool operator > (const Fn_iterator<Lhi,Fn>& li, const Fn_iterator<Rhi,Fn>& ri){
    return li.base() > ri.base();
}
template<typename Lhi,typename Rhi, typename Fn>
constexpr bool operator >= (const Fn_iterator<Lhi,Fn>& li, const Fn_iterator<Rhi,Fn>& ri){
    return li.base() >= ri.base();
}
template<typename Lhi,typename Rhi, typename Fn>
constexpr decltype(auto) operator - (const Fn_iterator<Lhi,Fn>& li, const Fn_iterator<Rhi,Fn>& ri){
    return li.base() - ri.base();
}

namespace dtl {
namespace itrt {

class Dereference_fn{
public:
    template<typename Tp>
    decltype (auto) operator() (Tp&& p) const{
        return *std::forward<Tp>(p);
    }
};

class Pair_second_fn{
public:
    template<typename Tp>
    auto& operator() (Tp&& p) const{
        return std::forward<Tp>(p).second;
    }
};

} // namespace itrt
} // namespace dtl

/// Iterator adaptor to iterate values of map-like containers (not key-value pairs).
template <typename Iterator>
using Pair_second_iterator = Fn_iterator<Iterator,dtl::itrt::Pair_second_fn>;

/// Iterator adaptor for a container that holds (smart) pointers to the object, not the objects by themselves.
template <typename Iterator>
using Inderection_iterator = Fn_iterator<Iterator,dtl::itrt::Dereference_fn>;

/// Iterator adaptor to iterate the last level of multilevel containers
template<typename Outer_iterator, typename ... Inner_iterators>
class Multilevel_iterator{
    static constexpr std::size_t last_level    = sizeof ... (Inner_iterators);
    using Tuple                 = std::tuple<Outer_iterator, Inner_iterators...>;
    using Innermost_iterator    = std::tuple_element_t<last_level, Tuple>;
public:
    // TODO: make biderectional version.
    using difference_type   = typename std::iterator_traits<Innermost_iterator>::difference_type;
    using value_type        = typename std::iterator_traits<Innermost_iterator>::value_type;
    using reference         = typename std::iterator_traits<Innermost_iterator>::reference;
    using pointer           = typename std::iterator_traits<Innermost_iterator>::pointer;
    using iterator_category = std::common_type_t<
        std::forward_iterator_tag,    // no random access, not biderectional
        typename std::iterator_traits<Outer_iterator>::iterator_category,
        typename std::iterator_traits<Inner_iterators>::iterator_category ... >;

    /*!
     * \brief Constructs the iterator
     * \param i_0, i_n      the underlying iterators
     * \param last          the past-the-end iterator for the i_o
     *
     * last should be stored to prevent dereferencing of the past-the-end iterator.
     */
    Multilevel_iterator(Outer_iterator i_0, Inner_iterators ... i_n , Outer_iterator last):
        it_(std::move(i_0), std::move(i_n) ... ), last_(last)
    {
        skip_invalid_forward();
    }

    reference operator* () const {
        return *it<last_level>();
    }

    pointer operator -> () const {
        return it<last_level>().operator ->();
    }

    Multilevel_iterator& operator ++ () {
        // assuming the innermost iterator is an incrementable iterator, i.e. valid.
        ++ it<last_level>();

        // if the innermost iterator is past-the-end now, we will skip forward to
        // validate the state of the iterator
        skip_invalid_forward();
        return *this;
    }
    Multilevel_iterator operator ++ (int) {
        Multilevel_iterator tmp = *this;
        operator ++ ();
        return tmp;
    }

    bool operator == ( const Multilevel_iterator& other ){
        if( !is_past_the_end() || !other.is_past_the_end() ) {
            return it_ == other.it_;
        }
        return it<0>() == other.it<0>();
    }

    bool operator != ( const Multilevel_iterator& other ){
        return ! (*this == other);
    }

private:
    template<std::size_t Ix>
    using Ix_tag    = std::integral_constant<std::size_t,Ix>;

    template<std::size_t Ix>
    auto& it() const{
        return std::get<Ix>(it_);
    }
    template<std::size_t Ix>
    auto& it(){
        return std::get<Ix>(it_);
    }

    template<std::size_t Ix>
    auto begin(Ix_tag<Ix>) const{
        using std::begin;
        return begin(*it<Ix-1>());
    }
    template<std::size_t Ix>
    auto end(Ix_tag<Ix>) const {
        using std::end;
        return end(*it<Ix-1>());
    }
    auto begin(Ix_tag<0>) const {
        return it<0>();
    }
    auto end(Ix_tag<0>) const {
        return last_;
    }

    template<std::size_t Ix>
    bool is_last() const {
        return begin(Ix_tag<Ix>{}) == end(Ix_tag<Ix>{});
    }
    // returns true, if the entire iterator is a past-the-end iterator.
    bool is_past_the_end() const {
        return is_last<0>();
    }

    void skip_invalid_forward(){
        skip_forward(false,Ix_tag<0>{});
    }

    // returns false if "inner" iterators still points to an empty range
    template<std::size_t Ix>
    bool skip_forward(bool reset, Ix_tag<Ix>) {
        if(reset) it<Ix>() = begin(Ix_tag<Ix>{});

        // the first iteration is special one.
        if(is_last<Ix>()) return false;
        if( skip_forward(reset,Ix_tag<Ix+1>{}) ) return true;
        ++ it<Ix>();

        for(;!is_last<Ix>(); ++it<Ix>()){
            if( skip_forward(true,Ix_tag<Ix+1>{}) ) return true;
        }
        return false;
    }
    bool skip_forward(bool, Ix_tag<last_level+1>) {
        return true;
    }

    using It_tuple  = std::tuple<Outer_iterator,Inner_iterators...>;
    It_tuple                it_;
    // to detect path-the-end iterator
    const Outer_iterator    last_;
};


/*!
 * \brief Proxy class that gives an access to begin/end methods of the underlying object of type T.
 *
 * Rationale: simplify getters for iterators to private containers.
 * Supports both const and non const range access (Range doesn't).
 */
template<typename T, typename Iterator = typename T::iterator, typename Const_iterator = typename T::const_iterator>
class Iterable_proxy{
public:

    using iterator          = Iterator;
    using const_iterator    = Const_iterator;

    constexpr const_iterator begin()    const   {
        using   std::begin;
        return  const_iterator(begin(host_));
    } // iterator may be an adaptor
    constexpr const_iterator end()      const   {
        using   std::end;
        return  const_iterator(end(host_));
    }
    constexpr const_iterator cbegin()   const   {
        return  begin();
    }
    constexpr const_iterator cend()     const   {
        return  end();
    }
    constexpr iterator begin()                  {
        using   std::begin;
        return  iterator(begin(host_));
    }
    constexpr iterator end()                    {
        using   std::end;
        return  iterator(end(host_));
    }

    constexpr Iterable_proxy(T& t): host_(t) {}
private:
    T& host_;
};

//---------------- Range ----------------//
namespace dtl {
namespace itrt {

template <typename Iterator, typename Sentinel>
struct Range_base {
    static constexpr bool supports_unequal = __cplusplus >= 201703L;
    static_assert ( supports_unequal || std::is_same<Iterator,Sentinel>::value,
        "a range have to have a type of a sentinel  be the same as a type of an iterator in versions of C++ before C++17");
};

} //  namespace itrt
} //  namespace dtl

/// Auxiliary class to replace begin*/end* methods with a single method that returns the range object.
template <typename Iterator, typename Sentinel = Iterator>
class Range: dtl::itrt::Range_base<Iterator, Sentinel> {
public:
    using iterator  = Iterator;
    using sentinel  = Sentinel;

    constexpr Range(iterator it, sentinel s):
        first_(it), last_(s)
    {}

    constexpr iterator begin()  { return first_; }
    constexpr sentinel end()    { return last_; }

private:
    iterator    first_;
    sentinel    last_;
};

/*
 * std::begin & std::end doesn't have version with rvalue-reference parameter and
 * can't call non-const begin()/end() of a temporary object, but Range is a proxy
 * class that can be such a temporary object.
*/
template <typename I, typename S>
auto begin(Range<I,S>&& range){
    return range.begin();
}
template <typename I, typename S>
auto end(Range<I,S>&& range){
    return range.end();
}

/// Auxiliary class to replace cbegin*/cend* methods with a single method that returns the range object.
template <typename Const_iterator, typename Const_sentinel = Const_iterator>
class Const_range: dtl::itrt::Range_base<Const_iterator, Const_sentinel> {
public:
    using const_iterator    = Const_iterator;
    using const_sentinel    = Const_sentinel;

    constexpr Const_range( const_iterator it, const_sentinel s):
        first_(it), last_(s)
    {}

    constexpr const_iterator begin()    const { return first_; }
    constexpr const_sentinel end()      const { return last_; }
    constexpr const_iterator cbegin()   const { return first_; }
    constexpr const_sentinel cend()     const { return last_; }
private:
    const_iterator  first_;
    const_sentinel  last_;
};

//---------------- end of Range ----------------//

template <typename T>
auto make_iterable_proxy(T& t){
    using std::begin;
    using std::cbegin;
    return Iterable_proxy<T, decltype (begin(t)), decltype (cbegin(t))> (t);
}

template<typename Iterator, typename Functor>
auto make_fn_iterator(Iterator wrapped, Functor fn){
    return Fn_iterator<Iterator,Functor>(std::move(wrapped), std::move(fn));
}

} // namespace dk

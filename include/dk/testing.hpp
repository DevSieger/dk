/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <iostream>

namespace dk {

/*!
 * \brief a class to test usage of special member functions by algorithms.
 * \tparam force_noexcept noexcept specification of move construction/asignment and swap function
 *
 * rationale: I have written similar classes surprisingly many times to check if
 * constructions/asignments are going well.
 */
template< bool force_noexcept>
class Sm_tester{
public:
    Sm_tester(){
        output()<<"default: "<<this<<"()"<<std::endl;
    }
    Sm_tester(const Sm_tester& o){
        output()<<"copy: "<<this<<"("<<&o<<")"<<std::endl;
    }

    // noexcept here may be inaccurate, but mandatory (hint: std::move_if_noexcept)
    Sm_tester(Sm_tester&&o) noexcept(force_noexcept){
        output()<<"move: "<<this<<"("<<&o<<")"<<std::endl;
    }

    ~Sm_tester(){
        output()<<"destructor: ~"<<this<<"()"<<std::endl;
    }

    Sm_tester& operator=(const Sm_tester& o){
        output()<<"copy: "<<this<<" = "<<&o<<std::endl;
        return *this;
    }

    Sm_tester& operator=(Sm_tester&& o) noexcept(force_noexcept){
        output()<<"move: "<<this<<" = "<<&o<<std::endl;
        return *this;
    }

    // adl swap
    friend void swap(Sm_tester& lhv, Sm_tester& rhv) noexcept(force_noexcept){
        output()<<"swap( "<<&lhv<<", "<<&rhv<<" )"<<std::endl;
    }

    static std::ostream& output(){ return *stream_ptr();}
    static void output(std::ostream& s){ stream_ptr()=std::addressof(s);}

private:
    static std::ostream*& stream_ptr(){
        static std::ostream* ptr = std::addressof(std::cout);
        return ptr;
    }
};

}

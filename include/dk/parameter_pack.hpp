/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <utility>
/*
 * purpose - to simplify work with type parameter packs.
 */
namespace dk {

/// Incomplite type to hold the type parameter pack.
template<typename ... Tps>
struct Pack;

namespace detail {

template<typename ... Tps>
struct Concat;

template<typename T, typename ... Tps>
struct Concat<T,Tps...>{
    using type = typename Concat<Pack<T>,Tps...>::type;
};

template<typename ... As, typename T, typename ... Tps>
struct Concat<Pack<As...>,T,Tps...>{
    using type = typename Concat<Pack<As...,T>,Tps...>::type;
};
template<typename ... As, typename ... Bs, typename ... Tps>
struct Concat<Pack<As...>,Pack<Bs...>,Tps...>{
    using type = typename Concat<Pack<As...,Bs...>,Tps...>::type;
};
template<typename ... As>
struct Concat<Pack<As...>>{
    using type = Pack<As...>;
};
template<>
struct Concat<>{
    using type = Pack<>;
};

template<template<typename ... T> class Tc, typename ... Tps>
struct Apply{
    using type = typename Apply<Tc,typename Concat<Tps...>::type>::type;
};
template<template<typename ... T> class Tc, typename ... Tps>
struct Apply<Tc,Pack<Tps...>>{
    using type = Tc<Tps...>;
};

template<typename W, typename ... Tps>
struct Exlude{
    using type = typename Exlude<W,typename Concat<Tps...>::type>::type;
};

template<typename W, typename ... Tps>
struct Exlude<W,Pack<W,Tps...>>{
    using type = typename Exlude<W,Pack<Tps...>>::type;
};

template<typename W, typename T0, typename ... Tps>
struct Exlude<W,Pack<T0,Tps...>>{
    using type = typename Concat<T0, typename Exlude<W,Pack<Tps...>>::type>::type;
};

template<typename W>
struct Exlude<W,Pack<>>{
    using type = Pack<>;
};

enum class Associativity:bool{
    l2r = false,        // (1+2)+3
    r2l = true          // 1 + (2+3)
};

template<Associativity asc,template<typename, typename> class F_tp, typename Init_p, typename ... Tps>
struct Fold{
    using type = typename Fold<asc, F_tp, typename Concat<Init_p, Tps...>::type>::type;
};

template<template<typename, typename> class F_tp, typename L_tp, typename R_tp, typename ... Tps>
struct Fold<Associativity::l2r,F_tp, Pack<L_tp, R_tp,Tps...>>{
    using type = typename Fold<Associativity::l2r, F_tp, Pack<F_tp<L_tp,R_tp>, Tps...>>::type;
};

template<template<typename, typename> class F_tp, typename L_tp, typename R_tp, typename ... Tps>
struct Fold<Associativity::r2l,F_tp, Pack<L_tp, R_tp,Tps...>>{
    using type = F_tp<L_tp, typename Fold<Associativity::r2l, F_tp, R_tp, Pack< Tps...>>::type>;
};

template<Associativity asc,template<typename, typename > class F_tp, typename Init_p>
struct Fold<asc,F_tp, Pack<Init_p> >{
    using type = Init_p;
};

template <template <typename> class Pred, typename ... Tps>
struct Match: std::integral_constant<std::size_t,Match<Pred,typename Concat<Tps...>::type>::value>{};

template <template <typename> class P, typename T, typename ... Tps>
struct Match<P,Pack<T,Tps...>>:
        std::integral_constant<std::size_t, Match<P,Pack<Tps...>>::value + P<T>::value >
{};

template <template <typename> class P>
struct Match<P,Pack<>>: std::integral_constant<std::size_t,0>{};

template <typename W>
struct Is_same_test{
    template<typename T>
    using Pred = std::is_same<W,T>;
};

template <std::size_t Ix, typename ... Tps>
struct At{
    using type = typename At<Ix, typename Concat<Tps...>::type>::type;
};

template <typename T, typename ... Tps>
struct At<0, Pack<T,Tps...>>{
    using type = T;
};

template <std::size_t Ix, typename T, typename ... Tps>
struct At<Ix, Pack<T,Tps...>>{
    using type = typename At<Ix-1, Pack<Tps...>>::type;
};

template <std::size_t Ix>
struct At<Ix, Pack<>>{
    static_assert ( Ix>=0, "out of range access to parameter pack" );
};

template <template <typename> class P, typename ... Tps>
struct Find:
        std::integral_constant<std::size_t, Find<P, typename Concat<Tps...>::type>::value>
{};

template <template <typename> class P,typename T, typename ... Tps>
struct Find<P, Pack<T,Tps...>>:
        std::integral_constant<std::size_t, (P<T>::value)? 0 : Find<P, Pack<Tps...> >::value+1>
{};

template <template <typename> class P>
struct Find<P, Pack<>>:
        std::integral_constant<std::size_t,0>
{};

template<typename Maping_pack, typename T>
struct Mapper;
template<typename F, typename S, typename ... Tps, typename T>
struct Mapper< Pack<Pack<F,S>,Tps...>, T>{
    using type = typename Mapper<Pack<Tps...>,T>::type;
};
template<typename S, typename ... Tps, typename T>
struct Mapper< Pack<Pack<T,S>,Tps...>, T>{
    using type = S;
};
template<typename T>
struct Mapper< Pack<>, T>{};

template<typename T>
struct Is_pack: std::false_type
{};

template <typename ... Tps>
struct Is_pack<Pack<Tps...>>: std::true_type
{};

template<typename ... Tps>
struct Pack_size: std::integral_constant<std::size_t,Pack_size<typename Concat<Tps...>::type>::value>{};

template<typename ...Tps>
struct Pack_size<dk::Pack<Tps...>>: std::integral_constant<std::size_t, sizeof...(Tps)> {};

}

/**
 * \brief Concatinates packs of types and usual types into a single Pack
 *
 * to pass Pack as an element wrap it using Pack:
 *  concat_t<Pack<bool,Pack<>>,int,Pack<Pack<>>> == Pack<bool,Pack<>,int,Pack<>>
 */
template<typename ... Tps>
using concat_t = typename detail::Concat<Tps...>::type;

/// Applies the given parameters to the given template, accepts Pack
template<template<typename ... T> class Tc, typename ... Tps>
using apply_t = typename detail::Apply<Tc,Tps...>::type;

/// Exludes the type W from the given parameters (types and Packs) and returns a single Pack
template<typename W, typename ... Tps>
using exlude_t = typename detail::Exlude<W,Tps...>::type;

/// returns F<Tp,F<Tps[0], ... F<Tps[N-2], F<Tps[N-1],Tps[N]>> ... >>> or Tp if Tps is empty
template<template<typename, typename> class F, typename Init_p, typename ... Tps>
using right_fold_t = typename detail::Fold<detail::Associativity::r2l,F,Init_p,Tps...>::type;

/// returns F<F< ... F< F<Tp,Tps[1]>, Tps[2]> ... , Tps[N-1]>,Tps[N]> or Tp if Tps is empty
template<template<typename, typename> class F, typename Init_p, typename ... Tps>
using left_fold_t = typename detail::Fold<detail::Associativity::l2r,F,Init_p,Tps...>::type;

/*
 *  you can use it to generate inheritance:
struct Ex_base{};

template<typename B,typename T>
struct Ex:B{
    T value_;
};

template<typename ... Tps>
using tuple_like = fold_t<Ex,Ex_base, Tps...>
*/

template<template<typename T, typename Base> class F_tp, typename Init_p, typename ... Tps>
using fold_t = left_fold_t<F_tp,Init_p,Tps...>;

/// returns Pred<Tps[0]>::value + Pred<Tps[1]>::value + ... + Pred<Tps[N]>::value
template <template <typename> class Pred, typename ... Tps>
using match = detail::Match<Pred,Tps...>;

/// returns the number of W types in Tps
template <typename W, typename ... Tps>
using get_count = detail::Match< detail::Is_same_test<W>::template Pred,Tps...>;

/// returns the N-th type in the concatinated parameter pack
template <std::size_t N, typename ... Tps>
using type_at_t = typename detail::At<N,Tps...>::type;

/// returns an index of the first W in the concatinated parameter pack or the size
/// of the pack if nothing is found.
template <typename W, typename ... Tps>
using find_in_pack = detail::Find<detail::Is_same_test<W>::template Pred,Tps...>;

/// returns an index of the first type T in the concatinated parameter pack for which
/// P<T>::value is true or the size of the pack if nothing is found.
template <template <typename> class P, typename ... Tps>
using find_in_pack_if = detail::Find<P,Tps...>;

/*
using Example_mapping_pack =
Pack<
    Pack<   short,                  std::int16_t>,
    Pack<   int,                    std::int32_t>,
    Pack<   long,                   std::int64_t>,
    Pack<   long long,              std::int64_t>,

    Pack<   unsigned short,         std::uint16_t>,
    Pack<   unsigned int,           std::uint32_t>,
    Pack<   unsigned long,          std::uint64_t>,
    Pack<   unsigned long long,     std::uint64_t>,

    Pack<   char,                   char>,
    Pack<   unsigned char,          unsigned char>,
    Pack<   signed char,            signed char>,
    Pack<   char16_t,               char16_t>,
    Pack<   char32_t,               char32_t>,

    Pack<   float,                  float>,
    Pack<   double,                 double>
>;
*/
/// provides class template 'result' to map type T using rules defined in the given Mapping_pack
template<typename Mapping_pack>
struct Type_map{
    template<typename T>
    using result = detail::Mapper<Mapping_pack,T>;
};


/// type trait to test if T is a specialization of Pack
template <typename T>
using is_pack = detail::Is_pack<T>;

template <typename F, template<typename> class Tag_type, typename ... Tps, typename ... Args>
auto pack_tag_apply(F f,Tag_type<Pack<Tps...>>, Args&&...args){
    return f(std::forward<Args>(args)...,Tag_type<Tps>{} ...);
}

/// returns the number of types in the concatinated pack (i.e. concat_t<Tps...>)
template <typename ... Tps>
using pack_size = detail::Pack_size<Tps...>;

}

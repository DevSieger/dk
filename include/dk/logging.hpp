/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <string>
#include <dk/format_string.hpp>
#include <functional>

/*
 * define DK_LOGGING_IMPLEMENTATION before including this file in the single translation unit,
 * and call dk::init_logging(path,date_format)
 */

namespace dk{

/// The threshold level of a log detalization.
enum class Detail_level: unsigned int{
    NONE,           ///< This value disables a logging.
    CRITICAL,       ///< Only critical errors will be logged.
    NON_CRITICAL,   ///< All errors will be logged.
    DEBUG,          ///< All errors and debug messages will be logged.
    DETAILED       ///< A verbose logging mode, all messages will be logged.
};

template<typename Char_tp, typename Allocator_tp = std::allocator<Char_tp>>
class Basic_logger{
public:
    using Char              = Char_tp;
    using allocator_type    = typename std::allocator_traits<Allocator_tp>::template rebind_alloc<Char>;
    using Char_traits       = std::char_traits<Char>;
    using Size              = std::size_t;

    using String            = std::basic_string<Char,std::char_traits<Char>,allocator_type>;
    using Output            = std::function<void(Detail_level, String&&, const std::locale&)>;
    using Format            = std::function<String(Detail_level, const Char*, Size, const std::locale& )>;

    /// Returns the current output functor
    const Output& output() const{
        return output_;
    }

    /// sets the new output functor
    Basic_logger& output(const Output& a_output){
        output_ = a_output;
        return *this;
    }

    /// Returns the current format functor
    const Format& format()const{
        return format_;
    }

    /// sets the new format functor
    Basic_logger& format(Format&& p_format){
        format_=std::move(p_format);
        return *this;
    }

    /// see void log(Detail_level lvl, const char* message, Args&& ... args)
    template<typename ... Args>
    void message(Detail_level lvl,const Char * message, Args&&...args){
        message_raw(lvl,message,Char_traits::length(message),std::forward<Args>(args)...);
    }

    /// see void log(Detail_level lvl, const char* message, Args&& ... args)
    template<typename Alloc_fp, typename Char_tr_fp, typename ... Args>
    void message(Detail_level lvl, const std::basic_string<Char,Char_tr_fp,Alloc_fp>& message, Args&&...args){
        message_raw(lvl,message.c_str(),message.size(),std::forward<Args>(args)...);
    }

    Detail_level level() const{
        return level_;
    }

    /// Sets the new detalization level
    ///
    /// Messages of more verbose level than the given one will be ignored
    Basic_logger& level(Detail_level lvl){
        level_ = lvl;
        return *this;
    }

    /// tests if the logger is enabled
    bool enabled()const{
        return enabled_;
    }

    /// enables the logger if enbld is true, disables otherwise
    Basic_logger& enabled(bool enbld){
        enabled_=enbld;
        return *this;
    }

    /// returns the current locale
    const std::locale& locale() const{
        return loc_;
    }

    /// sets a locale
    Basic_logger& locale(const std::locale& loc){
        loc_ = loc;
        return *this;
    }

    Basic_logger(Detail_level filter_level=Detail_level::DETAILED, bool initialy_enabled=true):
        level_(filter_level), enabled_(initialy_enabled)
    {}

private:
    void message_raw(Detail_level lvl, const Char* cstr, Size sz){
        if(accept(lvl)){
            output_(lvl,format_(lvl,cstr,sz,loc_),loc_);
        };
    }
    template<typename ... Args>
    void message_raw(Detail_level lvl, const Char* cstr, Size sz, Args&&...args){
        if(accept(lvl)){
            String rs =
                    generic_fstr<Char_tp, Char_traits, allocator_type>
                    (dk::Format_tag{}, loc_, cstr, sz, std::forward<Args>(args)...);
            output_(lvl,format_(lvl,rs.data(),rs.size(),loc_),loc_);
        };
    }

    bool accept(Detail_level lvl){
        return enabled() && lvl!=Detail_level::NONE && lvl <= level() && output_ && format_;
    }
    Output          output_;
    Format          format_;
    std::locale     loc_;
    Detail_level    level_;
    bool            enabled_;
};

using Logger=Basic_logger<char>;

/// gives an access to the global logger object
Logger& get_logger();

/*!
 * \brief Initializes the global logger object using the default output and format functors.
 * \param file_path     A path to a log file to write.
 * \param prefix_format A format of the date-time prefix. For the information about format specifiers see documentation of std::strftime.
 * \return true if an initialization succeed.
 * \details
 * The default output functor writes an incoming messages to the given file.
 * The default format functor adds a prefix with date and time of an incoming message in the specified format.
 */
bool init_logging(const char * path,const char * prefix_format="[%y.%m.%d %H:%M:%S]: ");

/*!
 * \brief Outputs the given message of the specified detailization level to the log.
 * \param   lvl         The specified detailization level.
 * \param   message     The given message.
 * \param   args        An argument to substitution.
 *
 * No logging is performed if lvl is more verbose that the detalization level of the global logger object or
 * if the logger is not enabled. At first the given message and args is processed using dk::generic_fstr.
 * Next the result is forwarded to the format functor of the logger which modifies it in the user defined
 * manner (adds timestamp by default). Then the result is sent to the output functor, which is responsible for
 * an actual output of the message (eg writes it in a file).
*/
template <typename ...Args>
inline void log(Detail_level lvl,const char* message, Args&& ... args){
    get_logger().message(lvl,message, std::forward<Args>(args)...);
}

/// see void log(Detail_level lvl,const char* message, Args&& ... args)
template<typename ...Args>
inline void log(Detail_level lvl, const std::string& message , Args...args){
    get_logger().message(lvl,message, std::forward<Args>(args)...);
}

}


#if defined(DK_LOGGING_IMPLEMENTATION)

#include <fstream>
#include <ctime>
#include <memory>
#include <sstream>
#include <iomanip>

namespace dk{

template <typename Char_tp,typename Alloc_tp>
class Date_time_formatter{
public:
    using Char      = Char_tp;
    using Logger    = Basic_logger<Char,Alloc_tp>;
    using String    = typename Logger::String;
    using Size      = typename Logger::Size;

    String operator()(Detail_level lvl, const Char* cstr, Size sz, const std::locale& loc){
        (void) lvl;
        time_t t=time(nullptr);
        tm ttm=*localtime(&t);
        std::basic_stringstream<Char, typename String::traits_type,Alloc_tp> ostr;
        ostr.imbue(loc);
        ostr<<std::put_time(&ttm,format_.c_str());
        ostr.write(cstr,sz);
        ostr<<std::endl;
        return ostr.str();
    }

    Date_time_formatter(const String& frmt):
        format_(frmt) {
    }

private:
    String      format_;
};

template <typename Char_tp,typename Alloc_tp>
class File_output{
public:
    using Char      = Char_tp;
    using Logger    = Basic_logger<Char,Alloc_tp>;
    using String    = typename Logger::String;
    using Size      = typename Logger::Size;

    File_output(const char * path):
        file_(path,std::ios_base::trunc|std::ios_base::out) {
    }

    bool opened()const{
        return file_.is_open();
    }

    void operator()(Detail_level lvl, String && str, const std::locale& loc){
        (void) lvl;
        if(file_.getloc()!=loc){
            file_.imbue(loc);
        }
        file_.write(str.data(),str.size());
        file_.flush();
    }

private:
    std::basic_ofstream<Char,typename String::traits_type> file_;
};

Logger& get_logger(){
    static Logger logger;
    return  logger;
};

bool init_logging(const char * path,const char * prefix_format){
    using String=std::string;
    using Alloc     = Logger::allocator_type;
    using Char      = Logger::Char;
    Logger& logger  = get_logger();

    auto output_ptr=std::make_shared<File_output<Char,Alloc>>(path);

    if(!output_ptr->opened()){
        logger.level(Detail_level::NONE);
        return false;
    };

    logger.output([ outp=std::move(output_ptr)](Detail_level lvl,String&& str,const std::locale& loc){
        (*outp)(lvl,std::move(str),loc);
    });

    auto format_ptr=std::make_shared<Date_time_formatter<Char,Alloc>>(prefix_format);

    logger.format([fmt=std::move(format_ptr)](Detail_level lvl,const char* cstr, std::size_t sz,const std::locale& loc){
        return (*fmt)(lvl,cstr,sz,loc);
    });

    return true;
}

}
#endif //DK_LOGGING_IMPLEMENTATION

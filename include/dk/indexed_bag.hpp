/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <dk/template_util.hpp>
#include <vector>
#include <utility>

namespace dk {

// for effictive removal, is is used for Octree
template<typename Value_type, typename Binding_functor, typename Index_type=std::size_t, template<typename...> class Container_type=std::vector>
class Indexed_bag: EBO_wrapper<Binding_functor,0>{
    using Binding_base=EBO_wrapper<Binding_functor,0>;
public:
    using Index=Index_type;
    using Value=Value_type;
    using Binding=Binding_functor;
    using Container=Container_type<Value>;

    Indexed_bag()=default;
    Indexed_bag(Binding const& binding):Binding_base(binding){}
    Indexed_bag(Binding && binding):Binding_base(std::move(binding)){}

    using Iterator = typename Container::iterator;
    using Const_iterator = typename Container::const_iterator;
    using Size = typename Container::size_type;

    void insert(Value const& v){
        emplace(v);
    }
    void insert(Value && v){
        emplace(std::move(v));
    }

    template<typename...Arg>
    void emplace(Arg&&...arg){
        data_.emplace_back(std::forward<Arg>(arg)...);
        // if a reallocation happens then it isn't necessary to rebind other elements,
        // because indices is preserved
        rebind(data_.size()-1);
    }

    /*
     * invalidates iterations!
    */
    void erase(Index ix){
        using std::swap;
        swap(data_[ix],data_.back());
        rebind(ix);
        data_.pop_back();
    }

    void clear(){
        data_.clear();
    }

    Iterator begin(){
        return data_.begin();
    }
    Iterator end(){
        return data_.end();
    }
    Const_iterator cbegin() const{
        return data_.cbegin();
    }
    Const_iterator cend()const{
        return data_.cend();
    }
    Const_iterator begin()const{
        return cbegin();
    }
    Const_iterator end()const{
        return cend();
    }

    Value& operator[](Index ix){
        return data_[ix];
    }

    Value const& operator[](Index ix)const{
        return data_[ix];
    }


    Size size()const{
        return data_.size();
    }

    bool empty()const{
        return data_.empty();
    }

    void swap(Indexed_bag & other){
        using std::swap;
        swap(static_cast<Binding_base&>(*this),static_cast<Binding_base&>(other));
        data_.swap(other.data_);
    }
    friend void swap(Indexed_bag & lhv,Indexed_bag & rhv){
        lhv.swap(rhv);
    }

private:
    void rebind(Index ix){
        Binding_base::get()(data_[ix],ix);
    }
    Container data_;
};

}

/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <utility>
#include <type_traits>

namespace dk{
namespace dtl{
namespace cmpr{

// lowercase style to look similar to std::*
// this is not real classes, they are like functions.
template<typename L,typename R, typename = bool>
struct is_comparable_less: std::false_type{};

template<typename L,typename R>
struct is_comparable_less<L,R, decltype (static_cast<bool>(std::declval<L>() < std::declval<R>()))>: std::true_type{};

template<typename L,typename R, typename = bool>
struct is_comparable_eq: std::false_type{};

template<typename L,typename R>
struct is_comparable_eq<L,R, decltype (static_cast<bool>(std::declval<L>() == std::declval<R>()))>: std::true_type{};

// helper to test if possible conversions are considered for an operand.
template<typename T>
struct Conversion_hlpr{
    // no definition
    operator T()const;
};

// helper to prevent use of conversions for a parameter
template<typename T>
class Conversion_blocker{
public:
    // constexpr?
    Conversion_blocker(const T& p_ref) noexcept: ref_(p_ref) {}
    operator const T& ()const noexcept { return ref_;}
private:
    const T& ref_;
};

// wraps L with Conversion_blocker if conversions disabled for the left operand of an operator tested with F.
template <template <typename,typename> class F,typename L,typename R>
using Lhv_param = std::conditional_t<F<Conversion_hlpr<L>,R>::value,L,Conversion_blocker<L>>;

// wraps R with Conversion_blocker if conversions disabled for the right operand of an operator tested with F.
template <template <typename,typename> class F,typename L,typename R>
using Rhv_param = std::conditional_t<F<L,Conversion_hlpr<R>>::value,R,Conversion_blocker<R>>;

// adaptors
template<typename L,typename R>
using cmpbl_l = is_comparable_less<L,R>;
template<typename L,typename R>
using cmpbl_eq = is_comparable_less<L,R>;

template<typename L, typename R>
class Comparison_ops{

    // handles possible conversions of arguments from Conversion_blocker and converts the result to bool
    static bool eq(const L& lhv, const R& rhv){
        return lhv==rhv;
    }
    static bool lt(const L& lhv, const R& rhv){
        return lhv<rhv;
    }
    static bool lt(const R& lhv, const L& rhv){
        return lhv<rhv;
    }

    // parameters to prevent unwanted conversions.
    // note: if both operands are different from both L and R then
    // no conversions possible, since operators become invisible for ADL.
    using L_LR_param = Lhv_param<cmpbl_l,const L&,const R&>;
    using L_RL_param = Rhv_param<cmpbl_l,const R&,const L&>;
    using L_Eq_param = Lhv_param<cmpbl_eq,const L&,const R&>;

    using R_LR_param = Rhv_param<cmpbl_l,const L&,const R&>;
    using R_RL_param = Lhv_param<cmpbl_l,const R&,const L&>;
    using R_Eq_param = Rhv_param<cmpbl_eq,const L&,const R&>;

public:

    /*
     * template operators like:
     *
     * template<typename L, typename R>
     * std::enableif_t<... , bool> operator != (L&& lhv, R&& rhv)
     *
     * are worse since they produces collisions and may generate operators
     * for types outside of the given set.
     */

    friend bool operator == (R_Eq_param lhv, L_Eq_param rhv){
        return eq(rhv,lhv);
    }
    friend bool operator != (L_Eq_param lhv, R_Eq_param rhv){
        return !eq(lhv,rhv);
    }
    friend bool operator != (R_Eq_param lhv, L_Eq_param rhv){
        return !eq(rhv,lhv);
    }
    friend bool operator >= (L_LR_param lhv, R_LR_param rhv){
        return !lt(lhv,rhv);
    }
    friend bool operator > (L_RL_param lhv, R_RL_param rhv){
        return lt(rhv,lhv);
    }
    friend bool operator <= (L_RL_param lhv, R_RL_param rhv){
        return !lt(rhv,lhv);
    }

    friend bool operator >= (R_RL_param lhv, L_RL_param rhv){
        return !lt(lhv,rhv);
    }
    friend bool operator > (R_LR_param lhv, L_LR_param rhv){
        return lt(rhv,lhv);
    }
    friend bool operator <= (R_LR_param lhv, L_LR_param rhv){
        return !lt(rhv,lhv);
    }

protected:
    ~Comparison_ops()=default;
};

template<typename T>
class Comparison_ops<T,T>{

    static bool eq(const T& lhv, const T& rhv){
        return lhv==rhv;
    }
    static bool lt(const T& lhv, const T& rhv){
        return lhv<rhv;
    }

    using L_lt_param = Lhv_param<cmpbl_l,const T&,const T&>;
    using R_lt_param = Rhv_param<cmpbl_l,const T&,const T&>;

    // == is symmetric
    using Eq_param = std::conditional_t<
        cmpbl_eq<T,Conversion_hlpr<T>>::value || cmpbl_eq<Conversion_hlpr<T>,T>::value,
        const T&, Conversion_blocker<const T&>>;
public:

    friend bool operator != (Eq_param lhv, Eq_param rhv){
        return !eq(lhv,rhv);
    }
    friend bool operator >= (L_lt_param lhv, R_lt_param rhv){
        return !lt(lhv,rhv);
    }
    friend bool operator > (R_lt_param lhv, L_lt_param rhv){
        return lt(rhv,lhv);
    }
    friend bool operator <= (R_lt_param lhv, L_lt_param rhv){
        return !lt(rhv,lhv);
    }

protected:
    ~Comparison_ops()=default;
};
}
}

/*!
 * \brief helper class to generate comparison operators for Target with every type from Others.
 *
 * It produces operators >,>=,<=,!=,== from operators == and <.
 *
 * usage: CRTP, it have to be inherited to make operators visible for ADL.
 * i.e:
 * template<typename T> class A: Comparable< A<T>,B,C,D>{};
 *
 * The following comparison operators should be defined for each type T in Others:
 *
 * bool operator == (const Target&, const T&)
 * bool operator <  (const Target&, const T&)
 * bool operator <  (const T&, const Target&)
 *
 * Implementation respects conversion rules, it does't add implicit conversions, but if both operands
 * are different from the original operand types then no conversions possible, since operators become
 * invisible for ADL.
 *
 * PS: I am sure that there is many implementations of similar classes, but I don't want to add dependencies.
 */
template<typename Target,typename ... Others>
class Comparable;

template<typename Tgt>
class Comparable<Tgt>{};

template<typename Tgt, typename Tp, typename ... Others>
class Comparable<Tgt,Tp,Others...>:
        // std::remove_const_t<std::remove_reference_t<Tgt>>, std::remove_const_t<std::remove_reference_t<Tp>>
        // here to ensure use of Comparison_ops<T,T> specialization.
        dtl::cmpr::Comparison_ops< std::remove_const_t<std::remove_reference_t<Tgt>>, std::remove_const_t<std::remove_reference_t<Tp>> >,
        Comparable<Tgt,Others...>{
};

}

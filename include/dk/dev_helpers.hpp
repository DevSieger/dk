/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <dk/template_util.hpp>

namespace dk {

/// Helper class that calls the given functor from the destructor
template <typename Functor_tp>
class Generic_guard: EBO_wrapper<Functor_tp,0> {
    using Wrapper = EBO_wrapper<Functor_tp,0>;
public:
    using Functor = Functor_tp;
    ~Generic_guard(){
        Wrapper::get()();
    }

    Generic_guard(const Functor& f): Wrapper(f) {}
    Generic_guard(Functor && f): Wrapper(std::move(f)) {}
    Generic_guard() = default;

    // the functor should not be called twice.
    // no copying, no moving.
    Generic_guard(Generic_guard&&)=delete;
};

 // Generic_guard doesn't have move constructor, it is not possible to use template
 // function to auto deduce Generic_guard template parameter.
#define DK_GUARD_LAMDA(var_name, lambda) \
    auto var_name##_lambda = lambda; \
    dk::Generic_guard<decltype(var_name##_lambda)> var_name{std::move(var_name##_lambda)};

#if __cplusplus >= 201703L
// c++17 code doesn't need DK_GUARD_LAMDA hack
template<typename F>
Generic_guard(F&&) -> Generic_guard<F>;
#endif

} // namespace dk

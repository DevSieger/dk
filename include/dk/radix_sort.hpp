/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <vector>
#include <cstddef>
#include <iterator>
#include <type_traits>

namespace dk{

template<typename Value_type, typename Key_type>
class Radix_sort{
public:
    using Key=Key_type;
    using Value=Value_type;

    /*
     * requirements:
     * Functor: Key (const Value&)
     * Value:   copy-assignable
     * Key:     integral
     */
    template<typename Iterator,typename Functor>
    void operator()(Iterator begin_it,Iterator end_it, Functor key){
        static_assert(std::is_same<typename std::iterator_traits<Iterator>::value_type,Value>::value,
                    "value_type of Iterator must be Radix_sort::Value");

        clear_offsets();
        for(auto it=begin_it;it!=end_it;++it){
            ++offsets_[get_bucket(key(*it),0)];
        }
        scan_offsets();

        const size_t total=offsets_[group_size()];

        va_.resize(offsets_[group_size()]);
        vb_.resize(offsets_[group_size()]);

        Value* input=&va_[0];
        Value* output=&vb_[0];

        for(auto it=begin_it;it!=end_it;++it){
            size_t dest=offsets_[get_bucket(key(*it),0)]++;
            output[dest]=*it;
        }

        for( size_t shift=step_size(); shift < sizeof(Key)*8 ; shift+=step_size() ){

            using std::swap;
            swap(input,output);

            clear_offsets();
            for(size_t ix=0;ix<total;++ix){
                ++offsets_[get_bucket(key(input[ix]),shift)];
            }
            scan_offsets();

            for(size_t ix=0;ix<total;++ix){
                size_t dest=offsets_[get_bucket(key(input[ix]),shift)]++;
                output[dest]=std::move(input[ix]);
            }
        }

        auto it=begin_it;
        for(size_t ix=0;ix<total;++ix,++it){
            *it = output[ix];
        }
    }

    Radix_sort():offsets_(group_size()+1){}

private:
    static constexpr size_t step_size(){return 8;}
    static constexpr size_t mask(){return (1<<step_size())-1;}
    static constexpr size_t group_size(){return 1<<step_size();}

    using Values_buffer=std::vector<Value>;
    using Offsets=typename std::vector<typename Values_buffer::size_type>;
    using Bucket_index=typename Offsets::size_type;

    void scan_offsets(){
        typename Offsets::value_type sum=0;
        for(auto & v:offsets_){
            auto tmp=v;
            v=sum;sum+=tmp;
        }
    }

    void clear_offsets(){
        for(auto& v:offsets_)v=0;
    }

    constexpr Bucket_index get_bucket(Key i, size_t shift){
        return ( i >> shift ) & mask();
    }

    // this buffers are data members to avoid reallocations.
    Values_buffer va_;
    Values_buffer vb_;
    Offsets offsets_;
};

}

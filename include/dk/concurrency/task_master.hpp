/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <mutex>
#include <deque>
#include <future>
#include <utility>
#include <tuple>
#include <dk/concurrency/stack.hpp>
#include <dk/template_util.hpp>
#include <functional>
#include <exception>

namespace dk {

// If you are using references in Args make sure they stay valid until the call of 'done'
template<typename Func_t, typename ... Args>
class Task_master
{

public:
    using Argument_pack = std::tuple<Args...>;
    using Exception_handler = std::function<void(std::exception_ptr)>;

    Task_master(const Func_t & f): m0_(f)
    {}
    Task_master(Func_t&& f): m0_(std::move(f))
    {}

    Task_master(Task_master&& other)=delete;

    bool launch(unsigned n_threads){
        Guard guard(mtx_);
        if(!futures_.empty()) return false;

        n_running_=n_threads;

        for(unsigned i=0;i<n_threads;++i){
            auto self = this;
            auto ftr = std::async(std::launch::async,[self](){self->work();});
            futures_.emplace_back(std::move(ftr));
        }
        return true;
    }

    //we have to copy anyway
    void add_task(Args... args){
        // move is wrong, argument may be l-value reference
        push(Argument_pack{std::forward<Args>(args)...});
        cv_.notify_one();
    }

    template<typename Iterator>
    void add_tasks(Iterator first, Iterator last){
        stack().push_range(first,last);
        cv_.notify_all();
    }

    /// waits for the tasks completion
    bool done(){
        Unique_lock lock(mtx_);
        if(futures_.empty() || done_){
            return false;
        }
        done_ = true;
        lock.unlock();

        cv_.notify_all();
        for(auto& ftr:futures_){
            ftr.get();
        }

        lock.lock();
        futures_.clear();
        done_ = false;
        return true;
    }

    /*!
     * \brief sets the exception handler to the given one
     *
     * when the functor throws an exception, this exception is passed to the exception handler.
     * if the exception handler is empty then any thrown exception is ignored.
     * the exception handler:
     * - shall not throw
     * - is not required to be thread safe
     */
    void exception_handler(Exception_handler handler){
        exc_handler_ = handler;
    }
    const Exception_handler& exception_handler() const{
        return exc_handler_;
    }

    ~Task_master(){
        done();
    }

private:
    using Func = EBO_wrapper<Func_t,0>;
    using Stack = Concurrent_stack<Argument_pack>;
    using Seq = std::index_sequence_for<Args...>;
    using Mutex = std::mutex;
    using Unique_lock = std::unique_lock<Mutex>;
    using Guard = std::lock_guard<Mutex>;
    using Optional_args = typename Stack::Optional_value;

    void push(Argument_pack&& v){
        stack().emplace(std::move(v));
    }

    Optional_args pop(){
        return stack().pop();
    }

    void work(){
        Unique_lock lock(mtx_,std::defer_lock);
        Optional_args p;
        while(true){
            lock.lock();
            --n_running_;
            while(!(p=pop())){
                if(!n_running_ && done_){
                    lock.unlock();
                    cv_.notify_all();
                    return;
                }
                cv_.wait_for(lock,std::chrono::milliseconds(500));
            }
            ++n_running_;
            lock.unlock();

            try {
                do{
                    invoke(*std::move(p));

                    // reset to avoid assignment.
                    // experimantal::optional does't have 'reset'
                    p=Optional_args{};
                    p=pop();
                } while(p);
            } catch (...) {
                if(exception_handler()){
                    lock.lock();
                    --n_running_;
                    try {
                        exception_handler()(std::current_exception());
                    } catch (...) {
                        //any thrown exception is ignored
                    }
                    ++n_running_;
                    lock.unlock();
                }
            }
        }
    }
    Func_t& get_f(){
        return m0_.Func::get();
    }
    Stack& stack(){
        return m0_.stack_;
    }

    void invoke(Argument_pack&& v){
        invoke_h(std::move(v),Seq());
    }

    template<std::size_t ...Ix>
    void invoke_h(Argument_pack&& v, std::index_sequence<Ix...>){
        get_f()(std::get<Ix>(std::move(v))...);
    }

    Mutex mtx_;
    std::condition_variable cv_;

    struct Packed: Func{
        using Func::Func;
        Stack stack_;
    } m0_;

    std::deque<std::future<void>> futures_;
    Exception_handler exc_handler_;
    unsigned n_running_;
    bool done_{false};

};

}

/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <utility>
#include <atomic>
#include <memory>

#if __cplusplus > 201402ul
#include <optional>
namespace dk::detail{
template<typename T>
using Optional = std::optional<T>;
}
#else
#include <experimental/optional>
#if __cpp_lib_experimental_optional >= 201411
namespace dk{ namespace detail {

template<typename T>
using Optional = std::experimental::optional<T>;

}}
#else
static_assert(false,"std::optional or std::experimental::optional required");
#endif
#endif

namespace dk {

template<typename T,typename Allocator = std::allocator<T>>
class Concurrent_stack{
public:
    using value_type=T;
    using Optional_value = detail::Optional<value_type>;
    using reference = std::add_lvalue_reference_t<value_type>;
    using const_reference = std::add_const_t<reference>;
    using pointer = std::add_pointer_t<value_type>;
    using allocator_type = Allocator;

    Concurrent_stack(const allocator_type& a = allocator_type()): m_(a) {}


    allocator_type get_allocator(){ return allocator_type(m_);}

    bool is_lock_free() const{
        return head_.is_lock_free() && m_.next_id_.is_lock_free();
    }

    template<typename ... Args>
    void emplace(Args && ... args){
        Node_ptr node = new_node(std::forward<Args>(args)...);
        Node_ptr old = head_.load(std::memory_order_relaxed);
        node.ptr->next=old;
        while(!head_.compare_exchange_weak(old,node,std::memory_order_release,std::memory_order_relaxed)){
            node.ptr->next=old;
        }
    }

    template<typename Iterator>
    void push_range(Iterator first, Iterator last){
        if(first==last) return;
        Node_ptr bottom = new_node(*first);
        ++first;
        Node_ptr top = bottom;
        for(;first!=last;++first){
            Node_ptr tmp = new_node(*first);
            tmp.ptr->next=top;
            top=tmp;
        }
        Node_ptr old = head_.load(std::memory_order_relaxed);
        bottom.ptr->next=old;
        while(!head_.compare_exchange_weak(old,top,std::memory_order_release,std::memory_order_relaxed)){
            bottom.ptr->next=old;
        }
    }

    bool empty()const{
        return !head_.load(std::memory_order_relaxed).ptr;
    }

    Optional_value pop(){
        Optional_value ret;
        Node_ptr old = head_.load(std::memory_order_acquire);

        if(!old.ptr) return ret;
        Node_ptr next = old.ptr->next;

        while(!head_.compare_exchange_weak(old,next,std::memory_order_acquire,std::memory_order_acquire)){
            if(old.ptr){
                next = old.ptr->next;
            }else{
                return ret;
            }
        };

        ret.emplace( std::move(old.ptr->t) );
        delete_node(old.ptr);
        return ret;
    }

    void clear(){
        Node* head = head_.exchange(new_ptr(nullptr),std::memory_order_acquire).ptr;
        while(head){
            Node* tmp = head;
            head = head->next.ptr;
            delete_node(tmp);
        }
    }

    ~Concurrent_stack(){
        clear();
    }

private:

    struct Node;

    struct Node_ptr{

        /*
         * all Node_ptr::id produced by some Stack object have to be unique,
         * so we want to disable the default constructor to make a hint,
         * but atomic exchange (in gcc) require it for some reason.
         */
        Node_ptr()=default;

        //passing atomic_size_t as parameter will constrain too much.
        Node_ptr(Node* raw_ptr, std::size_t uniq_id): ptr(raw_ptr), id(uniq_id){}
        Node* ptr       = nullptr;
        /*
         * there was a problem:
         * if a pointer has'nt changed it does'nt mean that the pointer still refer to the same object.
         * id is almost unique for every object and serves to solve the problem.
         */
        std::size_t id  = 0;
    };

    struct Node{


        // next_node to avoid too perfect forwarding
        template<typename ... Args>
        Node(std::size_t id, Args &&... args):
            t(std::forward<Args>(args)...), next{nullptr,id}
        {}

        // no copy & move
        Node(Node&&) = delete;

        value_type t;
        Node_ptr next;

    };

    using Alloc_traits = typename  std::allocator_traits<allocator_type>::template rebind_traits<Node>;
    using Real_alloc = typename Alloc_traits::allocator_type;

    /// produces Node_ptr with an unique id.
    Node_ptr new_ptr(Node* raw_ptr){
        return {raw_ptr, m_.next_id_.fetch_add(1,std::memory_order_relaxed)};
    }

    template<typename ... Args>
    Node_ptr new_node(Args && ... args){
        Node* dst = Alloc_traits::allocate(m_,1);
        Alloc_traits::construct(m_,dst,0,std::forward<Args>(args)...);
        return new_ptr(dst);
    }

    void delete_node(Node* n){
        Alloc_traits::destroy(m_,n);
        Alloc_traits::deallocate(m_,n,1);
    }

    static constexpr std::size_t cache_line_size(){
        return 64;
    }
    alignas(cache_line_size()) std::atomic<Node_ptr> head_ {Node_ptr{nullptr,0}};

    // IMO it is ok to share with allocator
    alignas(cache_line_size()) struct Packed: Real_alloc{
        Packed(): Real_alloc(){}
        Packed(const allocator_type& a): Real_alloc(a){}
        Packed(allocator_type &&a): Real_alloc(std::move(a)){}
        std::atomic_size_t next_id_ {std::size_t{1}};
    } m_;
};

}

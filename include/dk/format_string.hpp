/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <array>
#include <sstream>
#include <cassert>
#include <tuple>
#include <dk/helpers.hpp>

#if __cplusplus > 201402L
#include <string_view>
#endif

// to format floating points.
// we need std::isfinite to workaround lowercase nan when F is specified.
#include <cmath>

namespace dk {

namespace default_format_handlers {
// this is here for ADL with default handlers
struct Format_tag {};
} //  namespace default_format_handlers

namespace dtl {
namespace frmt{

template <typename UInteger,typename Char>
typename std::enable_if<std::is_integral<UInteger>::value,const Char*>::type
read_uint_raw(const std::ctype<Char>& ct, const Char* first, const Char* last, UInteger& out){
    const Char* end_num = ct.scan_not(std::ctype_base::digit,first,last);
    UInteger value = 0;
    for(;first!=end_num;++first){
        // this is safe for digits
        char n = ct.narrow(*first,'?');
        assert(n!='?');
        value*=10;
        value+=n-'0';
    }
    out=value;
    return end_num;
}

// default value is used in few places
template<typename Ch>
using default_ch_traits = std::char_traits<Ch>;

template<typename Ch>
using default_allocator = std::allocator<Ch>;

// no C++20 yet
template <typename T>
using remove_cvref_t    = std::remove_cv_t<std::remove_reference_t<T>>;

// helper class template to reduce the number of type template parameters that are
// shared by auxiliary functions
template <typename Char_tp,
          typename Traits_tp    = default_ch_traits<Char_tp>,
          typename Allocator_tp = default_allocator<Char_tp>>
struct Format_helper{

    using Traits    = Traits_tp;
    using Char      = Char_tp;
    using Allocator = Allocator_tp;
    using String    = std::basic_string<Char,Traits,Allocator>;
    using Size      = typename String::size_type;
    using Ostream   = std::basic_ostream<Char, Traits>;
    using Default_tag   = default_format_handlers::Format_tag;

    // Tests if it is possible to call format_handler for an object of type T using Tag.
    template<typename T, typename Tag, typename = bool>
    struct Has_handler: std::false_type {};

    template<typename T, typename Tag>
    struct Has_handler<T, Tag,
            decltype(
            static_cast<bool>(
                format_handler(
                    std::declval<Tag&>(), std::declval<Ostream&>(), std::declval<T&>(),
                    std::declval<const Char*>(), std::declval<const Char*>())))
    >: std::true_type{};

    // tag types to select the way to call format_handler (or not to call)
    // usual case: "forward" (using &) the format tag argument
    struct Use_arg {};

    // fallback: use the default format tag type, if a call using the tag argument
    // is invalid. this one may happen in the case of an ambiguous call.
    struct Use_default {};

    // the case if ADL can't find format_handler for both the argument and the default tag
    struct No_handler {};

    template<typename T, typename Tag>
    using Type_of_handling  =
        std::conditional_t< Has_handler<T,Tag>::value,
            Use_arg,
            std::conditional_t<Has_handler<T, Default_tag>::value,
                Use_default,
                No_handler
            >
        >;

    template<typename T, typename Tag>
    static bool handle_format( Use_arg, Tag& tag, Ostream& ostr, T& value, const Char* first, const Char* last){
        // an use of lvalue reference for the tag is intentional. IMO it is wrong
        // to pass an object using rvalue reference more than once.
        return format_handler(tag, ostr, value, first, last);
    }

    template<typename T>
    static bool handle_format( Use_default, Default_tag& tag, Ostream& ostr, T& value, const Char* first, const Char* last){
        return format_handler(tag, ostr, value, first, last);
    }

    // use "<<" if there is no handler, format options string should be empty
    template<typename T, typename Tag>
    static bool handle_format( No_handler, Tag&, Ostream& ostr, T& value, const Char* first, const Char* last){
        if( first == last){
            ostr << value;
            return true;
        }
        return false;
    }

    template<typename T, typename Tag>
    static bool handle_format(Tag& tag, Ostream& ostr, T& value, const Char* first, const Char* last){
        return handle_format(Type_of_handling<T,Tag>{}, tag, ostr, value, first, last);
    }

    template<typename Tag, typename ... Tps>
    static String format_impl(Tag&& tag_obj, const std::locale& loc, const Char* cstr, Size str_size, Tps&&...args){

        constexpr std::size_t num_args  = sizeof... (args);

        auto params     = std::tie(args...);
        std::basic_ostringstream<Char,Traits,Allocator> ost;
        ost.imbue(loc);

        struct Placeholder_params{
            Size        index   = 0;
            // the option string
            const Char* opt_first;
            const Char* opt_last;
        };

        auto output = [&params, &ost, &tag_obj]( const Placeholder_params& p){
            auto output     = [&ost, &p, &tag_obj](auto&& val) {
                return handle_format(tag_obj, ost, val, p.opt_first, p.opt_last);
            };
            return dk::apply_tuple_visitor<bool>(p.index, output, params);
        };

        const std::ctype<Char>& ct = std::use_facet<std::ctype<Char>>(loc);
        // special char
        const Char tag      = ct.widen('%');
        // : for options string
        const Char opt_tag  = ct.widen(':');

        // returns pointer to the character next to the closing tag or 'first' on failure
        auto parse_placeholder = [&ct,tag,opt_tag](const Char* first,const Char* last, Placeholder_params& out)
                                 -> const Char*
        {
            if(first+2 >= last) return  first;

            assert(*first == tag);
            const Char* begin_num   = first + 1;

            Placeholder_params param{};

            param.opt_last  = Traits::find( begin_num, last-begin_num, tag );
            // test if there is no closing tag
            if(!param.opt_last) return first;

            // all escaped tags was handled outside of this function
            assert(param.opt_last > begin_num);

            param.opt_first = read_uint_raw(ct, begin_num, param.opt_last, param.index);

            // test if there is no number
            if(param.opt_first == begin_num) return first;
            if(param.opt_first != param.opt_last){
                if(*param.opt_first == opt_tag){
                    ++param.opt_first;
                    // the option string shouldn't be empty if opt_tag presents
                    if(param.opt_first == param.opt_last) return first;
                } else {
                    // the rest of the placeholder is not an option
                    return first;
                }
            }

            // indexation starts from 1, not 0.
            --param.index;
            // overflow is handled outside of the function.

            out = param;

            // skip the closing tag
            return param.opt_last + 1;
        };


        const Char* cur     = cstr;
        const Char* last    = cstr+str_size;
        while(cur!=last){
            const Char* cur_end = Traits::find(cur,last-cur,tag);

            // output an escaped tag character
            while(cur_end && cur_end+1<last && cur_end[1]==tag){
                ost.write(cur,cur_end-cur+1);
                cur     = cur_end + 2;
                cur_end = Traits::find(cur,last-cur,tag);
            };

            if(!cur_end) cur_end = last;

            if(cur_end!=cur){
                ost.write(cur,cur_end-cur);
            }

            if(cur_end!=last){
                assert(*cur_end==tag);
                Placeholder_params param;
                cur = parse_placeholder(cur_end,last,param);
                if(cur!=cur_end){
                    if(! (param.index < num_args && output( param ) ) ) {
                        // placeholder is lexicaly correct, but unhandled.
                        assert(cur>cur_end);
                        ost.write(cur_end,cur-cur_end);
                    }
                }else{
                    // placeholder is incorrect, skip the tag character
                    // TODO: report error?
                    ost.write(cur_end,1);
                    cur = cur_end+1;
                }
            }else{
                cur = last;
            }
        }
        return ost.str();
    }
};

template<typename T, typename R, typename Tag = default_format_handlers::Format_tag>
using enable_if_tag     = std::enable_if_t<std::is_base_of<Tag, remove_cvref_t<T>>::value, R>;

} //  namespace frmt
} //  namespace dtl

/*!
 * \brief tag to use as a parameter of format_handler functions.
 *
 * Using tag taking form of the format functions you can replace default format handlers:
 * define a derived tag type, use it as a type of the first parameter for a new format
 * handler instead of the old tag, and pass that tag to the tag taking format function.
 * IMPORTANT: The new handler and the new tag should be associated in terms of ADL.
*/
using Format_tag    = default_format_handlers::Format_tag;

/// a canonical way to access a generic implementation of fstr from outside of this header
///
/// to use in other places of dk.
/// see fstr(const char* format, Args&&... args)
template <typename Char,
          typename Char_traits  = dtl::frmt::default_ch_traits<Char>,
          typename Allocator    = dtl::frmt::default_allocator<Char>,
          typename Tag, typename ... Args
          >
dtl::frmt::enable_if_tag<Tag, std::basic_string<Char,Char_traits,Allocator>>
generic_fstr(
        Tag&& tag, const std::locale& loc, const Char* format, std::size_t sz, Args&&... args
        )
{
    // RVO
    return dtl::frmt::Format_helper<Char, Char_traits, Allocator>::format_impl(
                std::forward<Tag>(tag), loc, format, sz,std::forward<Args>(args)...);
}

//---------------- tag-taking fstr functions ----------------//

// TODO: update description
/*!
 * \brief Produces a string from the arguments following the given format.
 * \param tag       the tag for format_handler functions
 * \param format    the given format
 * \param args      the arguments
 * \return the produced string
 *
 * This is yet another type-safe snprintf.
 * Instead of snprintf conversion specifications ("%f","%d" , etc) this one uses \%N%,
 * where N is an 1-based index of an argument. The arguments are printed using << operator
 * with appropriate specialization of std::basic_ostream as left operand.
*/
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag,std::string>
fstr(Tag&& tag, const char* format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format, std::char_traits<char>::length(format),
                         std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag, std::string>
fstr(Tag&& tag, const std::string& format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format.data(), format.size(),
                         std::forward<Args>(args)...);
}
/// see fstr(const char* format, Args&&... args)
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag, std::wstring>
wfstr(Tag&& tag, const wchar_t* format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format, std::char_traits<wchar_t>::length(format),
                         std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag, std::wstring>
wfstr(Tag&& tag, const std::wstring& format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format.data(), format.size(),
                         std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag, std::u32string>
u32fstr(Tag&& tag, const char32_t* format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format, std::char_traits<char32_t>::length(format),
                         std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag, std::u32string>
u32fstr(Tag&& tag, const std::u32string& format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format.data(), format.size(),
                         std::forward<Args>(args)...);
}

#if __cplusplus > 201402L
#include <string_view>
/// see fstr(const char* format, Args&&... args)
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag, std::string >
fstr(Tag&& tag, const std::string_view& format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format.data(), format.size(),
                         std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename Tag, typename ... Args>
dtl::frmt::enable_if_tag<Tag, std::wstring >
wfstr(Tag&& tag, const std::wstring_view& format, Args&&... args){
    return generic_fstr( std::forward<Tag>(tag), std::locale(),
                         format.data(), format.size(),
                         std::forward<Args>(args)...);
}
#endif

//----------- the end of tag-taking fstr functions -----------//
//---------------- fstr functions ----------------//

template <typename ... Args>
std::string fstr(const char* format, Args&&... args){
    return fstr(dk::Format_tag{}, format, std::forward<Args>(args)...);
}
/// see fstr(const char* format, Args&&... args)
template <typename ... Args>
std::string fstr(const std::string& format, Args&&... args){
    return fstr(dk::Format_tag{}, format, std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename ... Args>
std::wstring wfstr(const wchar_t* format, Args&&... args){
    return wfstr(dk::Format_tag{}, format, std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename ... Args>
std::wstring wfstr(const std::wstring& format, Args&&... args){
    return wfstr(dk::Format_tag{}, format, std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename ... Args>
std::u32string u32fstr(const char32_t* format, Args&&... args){
    return u32fstr(dk::Format_tag{}, format, std::forward<Args>(args)...);
}

/// see fstr(const char* format, Args&&... args)
template <typename ... Args>
std::u32string u32fstr(const std::u32string& format, Args&&... args){
    return u32fstr(dk::Format_tag{}, format, std::forward<Args>(args)...);
}

#if __cplusplus > 201402L

/// see fstr(const char* format, Args&&... args)
template <typename ... Args>
std::string fstr(const std::string_view& format, Args&&... args){
    return fstr(dk::Format_tag{}, format, std::forward<Args>(args)...);;
}

/// see fstr(const char* format, Args&&... args)
template <typename ... Args>
std::wstring wfstr(const std::wstring_view& format, Args&&... args){
    return wfstr(dk::Format_tag{}, format, std::forward<Args>(args)...);;
}
#endif

//----------- the end of fstr functions -----------//

namespace default_format_handlers {

/*!
 * \brief Helper function to handle format options for floating point types.
 * \param ostr          the stream to output to
 * \param value         the value to be outputed using the given format
 * \param first, last   the format options string, may be empty
 * \return returns true if the output is handled, false if it failed
 */
template<typename Ch, typename Traits, typename Float>
std::enable_if_t<std::is_floating_point<Float>::value, bool>
format_handler(
        Format_tag, std::basic_ostream<Ch, Traits>& ostr,
        Float value, const Ch* first, const Ch* last
){
    using IB        = std::ios_base;
    using Ussize    = std::make_unsigned_t<std::streamsize>;
    struct State{
        IB::fmtflags    flags;
        std::streamsize prec;
        std::streamsize width;
    };

    State old_state;

    // I REALLY don't want to include <regexp>, handwritten parsing.
    if(first != last){
        old_state.flags = ostr.flags();
        old_state.prec  = ostr.precision();
        old_state.width = ostr.width();

        // do not perform real operations until parsing is done
        State new_state = old_state;

        const std::ctype<Ch>& ct    = std::use_facet<std::ctype<Ch>>(ostr.getloc());
        const Ch dot_ch             = ct.widen('.');
        const Ch* cur               = first;

        auto set_bits   = [&new_state](IB::fmtflags new_flag){
            const IB::fmtflags mask = IB::floatfield | IB::uppercase;
            new_state.flags = (new_state.flags & ~mask) | (new_flag & mask);
        };

        // is it correct to use 0?
        // type of fmtflags is implementation defined
        IB::fmtflags zero   = IB::floatfield & ~IB::floatfield;

        // handle floating point format parameter
        if( !ct.is(std::ctype_base::digit,*cur) && *cur != dot_ch){

            // ct.widen can't be used in swich :/
            if      (*cur == ct.widen('f')) { set_bits( IB::fixed );        }
            else if (*cur == ct.widen('e')) { set_bits( IB::scientific );   }
            // floatfield = fixed | scientific, the same is used for hexfloat
            else if (*cur == ct.widen('a')) { set_bits( IB::floatfield );   }
            else if (*cur == ct.widen('g')) { set_bits( zero ); }
            // UPPERCASE versions
            else if (*cur == ct.widen('F')) { set_bits( IB::fixed       | IB::uppercase );  }
            else if (*cur == ct.widen('E')) { set_bits( IB::scientific  | IB::uppercase );  }
            else if (*cur == ct.widen('A')) { set_bits( IB::floatfield  | IB::uppercase );  }
            else if (*cur == ct.widen('G')) { set_bits( IB::uppercase ); }
            // an incorrect format character
            else return false;

            /*
             * workaround:
             * if "fixed" is specialized than "<<" operator outputs lowercase "nan"
             * and "infinity" anyway, "uppercase" flag doesn't matter.
             *
             * shouldn't hurt, AFAIK if the value isn't finite than
             * "fixed" / "scientific" / "hexfloat" doesn't metter
             */
            const IB::fmtflags fu   = IB::fixed | IB::uppercase;
            if( !std::isfinite(value) && ((new_state.flags & fu) == fu)  ){
                set_bits(IB::uppercase);
            }

            ++cur;
        } else {
            set_bits(zero);
        }

        // handle optional width parameter
        if( cur!=last ){
            if(ct.is(std::ctype_base::digit,*cur)){
                Ussize uwidth;
                const Ch* num_end = dtl::frmt::read_uint_raw(ct,cur,last,uwidth);
                // at least one digit has been read
                assert( num_end != cur );
                new_state.width = static_cast<std::streamsize>(uwidth);
                cur = num_end;
            }
        }

        // handle optional precision parameter
        if( cur!=last){
            // after width should be a precision or the end
            // after a dot should be at least one digit
            // test if format options are invalid
            if(*cur != dot_ch || last-cur < 2) return false;
            ++cur;

            Ussize  uperc;
            const Ch* num_end = dtl::frmt::read_uint_raw(ct,cur,last,uperc);

            // not a number
            if(num_end != last ) return false;

            new_state.prec = static_cast<std::streamsize>(uperc);
        }

        ostr.flags( new_state.flags );
        ostr.precision( new_state.prec );
        ostr.width( new_state.width );
    };

    ostr<<value;

    if(first != last){
        ostr.flags( old_state.flags );
        ostr.precision( old_state.prec );
        ostr.width( old_state.width );
    }
    return true;
}

// Helper function to handle format options for integer types.
template<typename Ch, typename Traits, typename Integer>
std::enable_if_t<std::is_integral<Integer>::value, bool>
format_handler(
        Format_tag, std::basic_ostream<Ch, Traits>& ostr,
        Integer value, const Ch* first, const Ch* last
){
    using Ssize     = std::streamsize;
    using Ussize    = std::make_unsigned_t<Ssize>;
    using IB        = std::ios_base;

    struct State{
        IB::fmtflags    flags;
        Ssize           width;
    };

    State old_state;

    if(first != last) {
        const auto& ct    = std::use_facet<std::ctype<Ch>>(ostr.getloc());
        old_state.flags = ostr.flags();
        old_state.width = ostr.width();

        State new_state = old_state;

        const Ch* cur   = first;

        auto set_bits   = [&new_state](IB::fmtflags new_flag){
            const IB::fmtflags mask = IB::basefield | IB::uppercase;
            new_state.flags = (new_state.flags & ~mask) | (new_flag & mask);
        };

        // is it correct to use 0?
        // type of fmtflags is implementation defined
        const IB::fmtflags zero   = IB::basefield & ~IB::basefield;

        if( *cur == ct.widen('#') ){
            new_state.flags |=  IB::showbase;
            ++cur;
        } else {
            new_state.flags &= ~IB::showbase;
        }

        if( !ct.is(std::ctype_base::digit, *cur) ){
            // ct.widen can't be used in swich :/
            if      (*cur == ct.widen('o')) { set_bits( IB::oct ); }
            else if (*cur == ct.widen('d')) { set_bits( IB::dec ); }
            else if (*cur == ct.widen('x')) { set_bits( IB::hex ); }
            else if (*cur == ct.widen('X')) { set_bits( IB::hex | IB::uppercase );}
            // an incorrect format character
            else return false;
            ++cur;
        } else {
            set_bits(zero);
        }

        // handle optional width parameter
        if( cur!=last ){
            Ussize uwidth;
            const Ch* num_end = dtl::frmt::read_uint_raw(ct,cur,last,uwidth);
            // not a number
            if(num_end != last ) return false;

            new_state.width = static_cast<Ssize>(uwidth);
        }

        ostr.flags( new_state.flags );
        ostr.width( new_state.width );
    };

    ostr<<value;

    if(first != last){
        ostr.flags( old_state.flags );
        ostr.width( old_state.width );
    }
    return true;
}

} //  namespace default_format_handlers

}

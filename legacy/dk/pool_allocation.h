/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <dk/allocate.h>
#include <utility>
#include <vector>
#include <memory>

namespace dk{


class Simple_pool{
public:
    Simple_pool(){
    }
    Simple_pool(size_t size_of_chunk):chunk_size_(size_of_chunk){}

    Simple_pool(Simple_pool&& other):
        chunks_(std::move(other.chunks_)),
        flist_head_(other.flist_head_),
        chunk_size_(other.chunk_size_),
        in_use_(other.in_use_),
        capacity_(other.capacity_),
        sig_(other.sig_)
    {
        other.chunks_.clear();
        other.flist_head_=nullptr;
        other.in_use_=0;
        other.capacity_=0;
    }

    Simple_pool& operator=(Simple_pool&& other){
        chunks_=std::move(other.chunks_);
        flist_head_=other.flist_head_;
        chunk_size_=other.chunk_size_;
        in_use_=other.in_use_;
        capacity_=other.capacity_;
        sig_=other.sig_;

        other.chunks_.clear();
        other.flist_head_=nullptr;
        other.in_use_=0;
        other.capacity_=0;
        return *this;
    }

    template<typename Value>
    void * allocate(){
        if(!flist_head_){
            expand_pool<Value>();
        }
        ++in_use_;
        return pop_next();
    }

    void deallocate(void * ptr){
        *(void**)ptr=flist_head_;
        flist_head_=ptr;
        --in_use_;
    }

    size_t in_use()const{
        return in_use_;
    }
    size_t capacity()const{
        return capacity_;
    }

    ~Simple_pool(){
        clear();
    }

private:

    template<typename Value>
    void expand_pool(){
        if(sig_.empty()){
            sig_.set<Value>();
        }
        expand_pool_typeless();
    }

    void expand_pool_typeless(){
        void * data=aligned_alloc(sig_.size()*chunk_size(),sig_.align());
        chunks_.push_back(data);
        char* dptr=static_cast<char*>(data);
        const char* const end=dptr+(chunk_size()-1)*sig_.size();
        for(;dptr!=end;dptr+=sig_.size()){
            *(void**)dptr=(void*)(dptr+sig_.size());
        }
        *(void**)dptr=nullptr;
        flist_head_=data;
        capacity_+=chunk_size();
    }

    void clear(){
        for(void* chunk:chunks_){
            aligned_free(chunk);
        }
    }

    void * pop_next(){
        void * ptr = flist_head_;
        flist_head_= *(void**)flist_head_;
        return ptr;
    }

    struct Signature{
        using Size=unsigned short;
        using Align=unsigned short;
        Size size_=0;
        Align align_=0;

        bool empty()const{
            return !size_;
        }
        Size size()const{
            return size_;
        }
        Align align()const{
            return align_;
        }
        template<typename Value>
        void set(){
            size_=std::max(sizeof(Value),sizeof(void*));
            align_=std::max(alignof(Value),alignof(void*));
        }
    };

    /* fields */
    std::vector<void*> chunks_;
    void * flist_head_=nullptr;
    size_t chunk_size_=1024;
    size_t in_use_=0;
    size_t capacity_=0;
    Signature sig_;

    size_t chunk_size()const{
        return chunk_size_;
    }
    /* fields end*/
};


//allocator
template<typename Value>
class Pool_allocator{
public:
    using value_type=Value;
    using pointer=value_type*;
    using const_pointer=const value_type*;
    using reference=value_type&;
    using const_reference=value_type const&;
    using difference_type=typename std::pointer_traits<pointer>::difference_type;
    using size_type=typename std::make_unsigned<difference_type>::type;
    using Traits=std::allocator_traits<Pool_allocator>;
    friend class Pool;


    static constexpr size_type max_size(){
        return 1;
    }

    template<typename New_value_type>
    struct rebind{
        using other=Pool_allocator<New_value_type>;
    };

    pointer allocate(size_type n){
        return static_cast<pointer>(pool_->allocate<value_type>());
    }

    void deallocate(pointer ptr,size_type n){
        pool_->deallocate(ptr);
    }

    friend bool operator ==(Pool_allocator const& lhs,Pool_allocator const& rhs){
        return &*lhs.pool_==&*rhs.pool_;
    }
    friend bool operator !=(Pool_allocator const& lhs,Pool_allocator const& rhs){
        return !(lhs==rhs);
    }


    template<typename New_value_type>
    Pool_allocator(Pool_allocator<New_value_type> const&  other): pool_(other.pool_)
    {
    }
    template<typename New_value_type>
    Pool_allocator(Pool_allocator<New_value_type> &&  other): pool_(other.pool_){
    }

    void destroy(pointer victim){
        victim->~value_type();
    }
    void construct(pointer ptr,const_reference target){
        new(static_cast<void*>(ptr)) value_type(target);
    }
private:
    template<typename Value_type> friend class Pool_allocator;
    using Pool_handle=std::shared_ptr<Simple_pool>;
    Pool_allocator(Pool_handle const& pool_handle):pool_(pool_handle){
    }

    Pool_handle pool_;
};

class Pool{
public:
    template<typename Value>
    using Allocator=Pool_allocator<Value>;

    template<typename Value>
    Allocator<Value> get_allocator(){
        return Allocator<Value>(pool_);
    }

    Pool(const Pool&)=delete;
    Pool& operator=(const Pool&)=delete;

    Pool(Pool&&)=default;
    Pool& operator=(Pool&&)=default;

    Pool(size_t chunk_size=1024){
        construct_pool(chunk_size);
    }

    size_t in_use()const{
        return pool_->in_use();
    }
    size_t capacity()const{
        return pool_->capacity();
    }

private:
    void construct_pool(size_t chunk_size){
        pool_=std::make_shared<Simple_pool>(chunk_size);
    }
    std::shared_ptr<Simple_pool> pool_;
};

}

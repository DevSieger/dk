/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <string>
#include <type_traits>

namespace dk{

namespace detail{
namespace pod_serialization{

template<typename ValueType,typename ResultType>
struct Serialization{
    using Value=ValueType;
    using Result=ResultType;

    static size_t serialized_size(Value const & obj){
        return sizeof obj;
    }
    static Result serialize(Value const & obj){
        Result ret;
        ret.resize(serialized_size(obj));
        using std::begin;
        using std::end;
        auto it=begin(ret);
        auto it_end=end(ret);
        serialize(obj,it,it_end);
        return ret;
    }

    //mutable forward iterator
    template<typename Iterator>
    static Iterator serialize(Value const & obj,Iterator begin,Iterator end){
        // CC36 (sam sebe zloi booratino, you are an evil booratino for you self, i.e it is your fault to hurt yourself, use with care)
        // static_assert(std::is_pod<Value>::value,"attempting serialize not POD type with serialize_pod");
        const char * ptr=reinterpret_cast<char const *>(&obj);
        const char * ptr_end=ptr + sizeof obj;
        for(; begin!=end && ptr!=ptr_end;++begin,++ptr){
            *begin=*ptr;
        }
        return begin;
    }

    static void deserialize(Value & out,Result const & in){
        using std::begin;
        using std::end;
        auto it=begin(in);
        auto it_end=end(in);
        deserialize(out,it,it_end);
    }

    //input iterator
    template<typename Iterator>
    static Iterator deserialize(Value & out,Iterator begin,Iterator end){
        // CC36
        // static_assert(std::is_pod<Value>::value,"attempting deserialize not POD type with serialize_pod");
        char * ptr=reinterpret_cast<char *>(&out);
        char * ptr_end=ptr + sizeof out;
        for(;begin!=end && ptr!=ptr_end;++begin,++ptr){
            *ptr=*begin;
        }
        return begin;
    }
};

template<typename ElementType,typename ResultType, typename Container>
struct ContainerSerialization{
    using Value=Container;
    using Element=ElementType;
    using Result=ResultType;
    using ElementSerialization=Serialization<Element,Result>;

    static size_t serialized_size(Value const & obj){
        return obj.size()*sizeof(Element);
    }

    static Result serialize(Value const & obj){
        Result ret;
        ret.resize(serialized_size(obj));
        using std::begin;
        using std::end;
        auto it=begin(ret);
        auto it_end=end(ret);
        serialize(obj,it,it_end);
        return ret;
    }
    template<typename Iterator>
    static Iterator serialize(Value const & obj,Iterator begin,Iterator end){
        for(Element const & e: obj){
            begin=ElementSerialization::serialize(e,begin,end);
        }
        return begin;
    }

    static void deserialize(Value & out,Result const & in){
        using std::begin;
        using std::end;
        auto it=begin(in);
        auto it_end=end(in);
        deserialize(out,it,it_end);
    }
    template<typename Iterator>
    static Iterator deserialize(Value & out,Iterator begin,Iterator end){
        while(begin!=end){
            Element e;
            begin=ElementSerialization::deserialize(e,begin,end);
            out.push_back(std::move(e));
        }
        return begin;
    }
};


}
}

template<typename T>
std::string serialize_pod(T const & obj){
    return detail::pod_serialization::Serialization<T,std::string>::serialize(obj);
}

template<typename T,typename E = typename T::value_type>
std::string serialize_cont(T const & obj){
    return detail::pod_serialization::ContainerSerialization<E,std::string,T>::serialize(obj);
}

template<typename T>
void deserialize_pod(T & out,std::string const & str){
    detail::pod_serialization::Serialization<T,std::string>::deserialize(out,str);
}

template<typename T,typename E = typename T::value_type>
void deserialize_cont(T & out,std::string const & str){
    detail::pod_serialization::ContainerSerialization<E,std::string,T>::deserialize(out,str);
}


template<typename T,typename Iterator>
Iterator serialize_pod(T const & obj,Iterator begin,Iterator end){
    return detail::pod_serialization::Serialization<T,std::string>::serialize(obj,begin,end);
}

template<typename T,typename Iterator>
Iterator serialize_cont(T const & obj,Iterator begin,Iterator end){
    return detail::pod_serialization::ContainerSerialization<typename T::value_type,std::string,T>::serialize(obj,begin,end);
}

template<typename T,typename Iterator>
Iterator deserialize_pod(T & out,Iterator begin,Iterator end){
    return detail::pod_serialization::Serialization<T,std::string>::deserialize(out,begin,end);
}

template<typename T,typename Iterator>
Iterator deserialize_cont(T & out,Iterator begin,Iterator end){
    return detail::pod_serialization::ContainerSerialization<typename T::value_type,std::string,T>::deserialize(out,begin,end);
}

}

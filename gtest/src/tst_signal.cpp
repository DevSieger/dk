/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <algorithm>
#include <dk/signal.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <tuple>
#include <vector>

namespace dk_unit_test {

using ::testing::ExpectationSet;
using ::testing::InvokeWithoutArgs;
using ::testing::Mock;
using ::testing::Return;
using ::testing::Sequence;
using ::testing::Test;
using ::testing::TestWithParam;
using ::testing::Throw;
using ::testing::Values;
using ::testing::ValuesIn;
using ::testing::WithParamInterface;
using ::testing::_;

namespace { // anonymous ns

template< typename Signature>
class Mock_functor;

template< typename R, typename ... A>
class Mock_functor<R(A...)>{
public:
    MOCK_METHOD(R, call, (A...));
};

class SignalTestBase: public Test
{
public:
    using Arg       = int;
    using Signature = void(Arg);
    using Signal    = dk::Signal<Signature>;
    using Handles   = std::vector<Signal::Handle>;
    using Mock_fn   = Mock_functor<Signature>;
    using Mocks     = std::vector<std::unique_ptr<Mock_fn>>;
    using Size      = std::size_t;
    using Functor   = Signal::Functor;

    SignalTestBase(Size sz):
        size_(sz)
    {
        handles_.reserve( size() );
        moc_fns_.reserve( size() );
        for(Size i = 0; i < size(); ++i){
            moc_fns_.push_back( std::make_unique<Mock_fn>() );
        }
    }

    void SetUp() override {
        for(Size i = 0; i < size(); ++i){
            auto h  = sig_.add( [this,i](Arg arg){ moc_fns_[i]->call(arg); } );
            handles_.push_back(h);
        }
    }


    Size size() const {
        return size_;
    }

    auto expect_call_all(Arg value, int times = 1){
        ExpectationSet es;
        for(auto& fn: moc_fns_){
            es  += EXPECT_CALL(*fn,call(value)).Times(times);
        }
        return es;
    }

    // Tests common effects of clear / hard_clear.
    void test_clear(bool hard_version){
        for(auto& fn: moc_fns_){
            EXPECT_CALL(*fn,call( _ )).Times(0);
        }

        if(hard_version){
            sig_.hard_clear();
        } else {
            sig_.clear();
        }

        EXPECT_EQ( sig_.size(), 0 )
                << "the number of the signal callbacks is non zero after clearing";
        EXPECT_TRUE(sig_.empty())
                << "the signal is non empty after clearing";

        // is it actually cleared?
        sig_(1);
    }

protected:
    Signal      sig_;
    Handles     handles_;
    Mocks       moc_fns_;

private:
    const Size  size_;
};

class SignalTestVariableSize:
        public WithParamInterface<std::size_t>,
        public SignalTestBase
{
public:
    SignalTestVariableSize():
        SignalTestBase(GetParam())
    {}
};

using Signal_test_param = std::tuple<std::size_t, std::size_t>;
class SignalTestVariableSizeAndTarget: public WithParamInterface<Signal_test_param>, public SignalTestBase
{
public:

    SignalTestVariableSizeAndTarget():
        SignalTestBase(std::get<0>(GetParam()))
    {}

    Size target() const {
        return std::get<1>(GetParam());
    }

    // is placing test code here so bad? is code copying is better? I don't think so.
    // Tests common effects of remove / hard_remove.
    void test_removal(bool hard_version){
        const Arg arg   = 3;

        //expect_call_all(arg);
        for(Size i = 0; i < size(); ++i){
            if( i!= target() )
                EXPECT_CALL(*moc_fns_[i], call(arg));
        }
        EXPECT_CALL(*moc_fns_[target()],call(_)).Times(0);

        auto h  = handles_[target()];
        if(hard_version){
            sig_.hard_remove(h);
        } else {
            sig_.remove(h);
        }

        EXPECT_EQ( sig_.size(), size()-1 )
                << "the number of the signal callbacks is invalid after the callback "<<target()<<" removal";

        // is it actually removed?
        sig_(arg);
    }

    void test_clear_within_emission(bool hard_version){
        const Arg arg0   = 4;
        const Arg arg1   = 5;

        for(Size i = 0; i < size(); ++i){
            auto& mf    = *moc_fns_[i];
            auto&& e1   = EXPECT_CALL(mf, call(arg0));

            if( hard_version && i > target()){
                e1.Times(0);
            }
            // Times(1) by default

            if( i == target() ){
                auto clear_and_check = [this,hard_version](){
                    if(hard_version){
                        sig_.hard_clear();
                    } else {
                        sig_.clear();
                    }

                    EXPECT_TRUE(sig_.empty());
                    sig_.emit(arg1);
                };
                e1.WillOnce(InvokeWithoutArgs(clear_and_check));
            }
        }
        sig_.emit(arg0);
    }
};

using Signal_test_param_2t = std::tuple<std::size_t, std::size_t, std::size_t>;

class SignalTestVariableSizeAnd2Targets:
        public WithParamInterface<Signal_test_param_2t>, public SignalTestBase
{
public:

    SignalTestVariableSizeAnd2Targets():
        SignalTestBase(std::get<0>(GetParam()))
    {}

    Size target_0() const {
        return std::get<1>(GetParam());
    }
    Size target_1() const {
        return std::get<2>(GetParam());
    }

    void test_removal_within_emission(bool hard_version) {
        const Arg arg0   = 4;
        const Arg arg1   = 5;

        for(Size i = 0; i < size(); ++i){
            auto& mf    = *moc_fns_[i];
            auto&& e1   = EXPECT_CALL(mf, call(arg0));

            if( hard_version && i==target_1() && target_0() < target_1() ){
                e1.Times(0);
            }
            // Times(1) by default

            if( i == target_0() ){
                auto remove_and_check = [this,hard_version](){
                    auto h  = handles_[target_1()];

                    if(hard_version){
                        sig_.hard_remove(h);
                    } else {
                        sig_.remove(h);
                    }

                    EXPECT_EQ(sig_.size(), size()-1);
                    sig_.emit(arg1);
                };
                e1.WillOnce(InvokeWithoutArgs(remove_and_check));
            }

            auto&& e2   = EXPECT_CALL(mf,call(arg1));
            e2.Times( i==target_1() ? 0 : 1 );
            e2.After(e1);
        }

        sig_.emit(arg0);
    }
};

auto make_parameters(std::size_t max_size){
    std::vector<Signal_test_param> ret;
    using Sz    = std::size_t;
    for( Sz i=1; i<=max_size; ++i){
        for(Sz j=0; j<i; ++j){
            ret.push_back({i,j});
        }
    }
    return ret;
}

auto make_parameters_2t(){
    std::vector<Signal_test_param_2t> ret;
    using Sz    = std::size_t;
    for( Sz i=1; i<=2; ++i){
        for(Sz j=0; j<i; ++j){
            for(Sz k=0; k<i; ++k){
                ret.push_back({i,j,k});
            }
        }
    }

    // 1 - 2
    ret.push_back({3,0,2});
    // 2 - 1
    ret.push_back({3,2,0});
    // - 1/2 -
    ret.push_back({3,1,1});

    // - 1 2 -
    ret.push_back({4,1,2});
    // - 2 1 -
    ret.push_back({4,2,1});
    // - 1 - 2 -
    ret.push_back({5,1,3});
    // - 2 - 1 -
    ret.push_back({5,3,1});
    return ret;
}

} // anonymous ns end
INSTANTIATE_TEST_SUITE_P(generic, SignalTestVariableSize,
                        Values(0,1,2,3,16));
INSTANTIATE_TEST_SUITE_P(generic, SignalTestVariableSizeAndTarget,
                        ValuesIn( make_parameters(4)));
INSTANTIATE_TEST_SUITE_P(generic, SignalTestVariableSizeAnd2Targets,
                        ValuesIn( make_parameters_2t()) );

TEST(SignalTest, add){
    using basic_cb    = void();
    dk::Signal<basic_cb> sig;

    EXPECT_TRUE(sig.empty())
            << "non empty default constructed signal";
    EXPECT_EQ(sig.size(), 0)
            << "the size of default constructed signal isn't zero";

    auto valid_handle_1 = sig.add([](){});

    EXPECT_NE(valid_handle_1, sig.invalid_handle())
            << "Signal::add returns an invalid handle";

    EXPECT_EQ(sig.size(), 1)
            << "Signal::add doesn't increase the number of callbacks";
    EXPECT_FALSE(sig.empty())
            << "signal is empty after a callback has been added";
}

TEST(SignalTest, emissionRunning){
    dk::Signal<void()> sig;

    auto closure    = [&sig](){
        EXPECT_TRUE(sig.emission_running())
                << "emission_running() != true when emitting";
    };
    sig.add(closure);

    EXPECT_FALSE(sig.emission_running())
            << "emission_running() != false when not emitting";

    sig.emit();
}

TEST(SignalTest, moveConstructor){
    EXPECT_FALSE(std::is_move_constructible<dk::Signal<void()>>::value);
}

TEST(SignalTest, moveAssignment){
    EXPECT_FALSE(std::is_move_assignable<dk::Signal<void()>>::value);
}

TEST(SignalTest, copyConstructor){
    EXPECT_FALSE(std::is_copy_constructible<dk::Signal<void()>>::value);
}

TEST(SignalTest, copyAssignment){
    EXPECT_FALSE(std::is_copy_assignable<dk::Signal<void()>>::value);
}

TEST_P(SignalTestVariableSize, call){
    expect_call_all(1);
    sig_(1);
}

TEST_P(SignalTestVariableSize, emit){
    expect_call_all(2);
    sig_.emit(2);
}

TEST_P(SignalTestVariableSize, size){
    EXPECT_EQ(sig_.size(), size());
    if(size() > 0){
        EXPECT_FALSE(sig_.empty());
    } else {
        EXPECT_TRUE(sig_.empty());
    }
}

TEST_P(SignalTestVariableSizeAndTarget, remove){
    test_removal(false);
}

TEST_P(SignalTestVariableSizeAndTarget, hardRemove){
    test_removal(true);
}

TEST_P(SignalTestVariableSize, clear){
    test_clear(false);
}
TEST_P(SignalTestVariableSize, hardClear){
    test_clear(true);
}

TEST_P(SignalTestVariableSize, invocationOrder){
    Sequence seq1;
    for(auto& fn_ptr: moc_fns_){
        EXPECT_CALL(*fn_ptr, call(1)).InSequence(seq1);
    }
    sig_.emit(1);
}

TEST_P(SignalTestVariableSizeAndTarget, recursiveEmission){
    const Arg arg0 = 1, arg1 = 2;

    auto invoke_fn  = [this]() { sig_.emit(arg1); };

    for(Size i = 0; i < size(); ++i){
        auto&& e =  EXPECT_CALL(*moc_fns_[i], call(arg0));
        if( i == target() ) e.WillOnce(InvokeWithoutArgs(invoke_fn));
        EXPECT_CALL(*moc_fns_[i], call(arg1)).After(e);
    }
    sig_(arg0);
}

TEST_P(SignalTestVariableSizeAndTarget, addWithinEmit){
    Mock_fn new_mock_fn;
    const Arg arg0   = 4;
    const Arg arg1   = 12;

    for(Size i = 0; i < size(); ++i){
        auto& mf    = *moc_fns_[i];
        auto&& e1   = EXPECT_CALL(mf, call(arg0));

        // Times(1) by default

        if( i == target() ){
            auto fn = [this, &new_mock_fn](){
                sig_.add([&new_mock_fn](Arg a){ new_mock_fn.call(a); });
                EXPECT_EQ(sig_.size(), size() + 1);
                sig_.emit(arg1);
            };

            e1.WillOnce(InvokeWithoutArgs(fn));

            EXPECT_CALL(new_mock_fn, call(arg1)).After(e1);
        }

        EXPECT_CALL(mf, call(arg1)).After(e1);
    }

    sig_.emit(arg0);
}

TEST_P(SignalTestVariableSizeAndTarget, clearWithinEmit){
    test_clear_within_emission(false);
}
TEST_P(SignalTestVariableSizeAndTarget, hardClearWithinEmit){
    test_clear_within_emission(true);
}

TEST_P(SignalTestVariableSizeAnd2Targets, removeWithinEmit){
    test_removal_within_emission(false);
}
TEST_P(SignalTestVariableSizeAnd2Targets, hardRemoveWithinEmit){
    test_removal_within_emission(true);
}

//---------------- CollectorTesting ----------------//

namespace  { // anonymous ns

template<typename Return_tp, typename Call_result_tp>
class Mock_collector_base;

// use a default constructor of derived classes to set up expectations
template<typename Return_tp, typename Call_result_tp>
class Mock_collector_base{
public:
    using Result    = Return_tp;

    Mock_collector_base() = default;
    Mock_collector_base(Mock_collector_base&&) = delete;

    MOCK_METHOD(bool, collect, (Call_result_tp&&));
    MOCK_METHOD(Return_tp, get, ());
};

template<typename Return_tp>
class Mock_collector_base<Return_tp, void> {
public:
    using Result    = Return_tp;

    Mock_collector_base() = default;
    Mock_collector_base(Mock_collector_base&&) = delete;

    MOCK_METHOD(bool, collect, ());
    MOCK_METHOD(Return_tp, get, ());
};

template <typename Real_class>
struct Collector_adaptor{
    template<typename T>
    using Result = Real_class;
};

class SignalCollectorVarSize: public TestWithParam<std::size_t>
{
public:
    std::size_t size() const {  return GetParam();  }
};

} // anonymous ns end

INSTANTIATE_TEST_SUITE_P(generic, SignalCollectorVarSize,
                        Values(0,1,5));

TEST_P(SignalCollectorVarSize, collectAndGet) {
    using Sz = std::size_t;

    // I want to use something not so trivial
    using Return_type   = std::vector<int>;

    // a hack to communicate with a default constructor.
    // GoogleTest is using only one thread, so this should be ok.
    static Sz num_callbacks = 0;

    // this one we need anyway.
    static Sz num_created   = 0;

    class TestingCollector:
            public Mock_collector_base<Return_type, Sz>{
    public:

        static Return_type expected_result() {
            return {1,2,3};
        }

        TestingCollector(){
            if( num_callbacks == 0 ) {
                EXPECT_CALL(*this, collect(_)).Times(0);
            }

            ExpectationSet e;

            for(Sz i = 0; i < num_callbacks; ++i){
                e += EXPECT_CALL(*this, collect(std::move(i)))
                        .WillOnce(Return(true));
            }

            EXPECT_CALL(*this, get())
                    .After(e)
                    .WillOnce(Return(expected_result()));

            ++ num_created;
        }
    };

    using Signal = dk::Signal< Sz(), Collector_adaptor<TestingCollector>::Result>;
    Signal sig;

    for(Sz i = 0; i < size(); ++i){
        sig.add( [i](){return i;} );
    }

    num_created     = 0;
    num_callbacks   = size();

    EXPECT_EQ(sig.emit(), TestingCollector::expected_result() );
    EXPECT_EQ(num_created, 1);
}

TEST(SignalCollector, breakLoop) {
    using Sz            = std::size_t;
    using Return_type   = int;

    constexpr Sz total_callbacks = 5;
    constexpr Sz break_at = 3;

    class TestingCollector:
            public Mock_collector_base<Return_type, int>{
    public:

        TestingCollector(){
            auto && e = EXPECT_CALL(*this, collect(1));
            for(Sz i = 1; i < break_at; ++i){
                        e.WillOnce(Return(true));
            }
            e.WillOnce(Return(false));

            EXPECT_CALL(*this, get())
                    .After(e);
        }
    };

    using Signal =
        dk::Signal<
            int(),
            Collector_adaptor<TestingCollector>::Result >;

    Signal sig;

    for(Sz i = 0; i < total_callbacks; ++i){
        sig.add( [](){return 1;} );
    }

    sig.emit();
}

// tests the case when callback return type is void
TEST(SignalCollector, voidArgument) {
    using Sz            = std::size_t;
    using Return_type   = Sz;

    constexpr Sz total_callbacks    = 5;
    constexpr Sz expected_result    = 12;

    class TestingCollector:
            public Mock_collector_base<Return_type, void>{
    public:
        TestingCollector(){
            auto && e = EXPECT_CALL(*this, collect() )
                        .Times(total_callbacks)
                        .WillRepeatedly(Return(true));
            EXPECT_CALL(*this, get())
                    .After(e)
                    .WillOnce(Return(expected_result));
        }
    };

    using Signal =
        dk::Signal<
            void(),
            Collector_adaptor<TestingCollector>::Result >;
    Signal sig;

    for(Sz i = 0; i < total_callbacks; ++i){
        sig.add( [](){} );
    }

    EXPECT_EQ( sig.emit(), expected_result );
}

// tests the case when collector result type is void
TEST(SignalCollector, voidResult) {
    using Sz    = std::size_t;

    constexpr Sz total_callbacks    = 5;
    constexpr int cb_result         = 12;

    class TestingCollector: public Mock_collector_base<void, int>
    {
    public:
        TestingCollector() {
            int arg = cb_result;

            auto && e = EXPECT_CALL(*this, collect(std::move(arg)) )
                        .Times(total_callbacks)
                        .WillRepeatedly(Return(true));

            EXPECT_CALL(*this, get())
                    .After(e);
        }
    };

    using Signal =
        dk::Signal<
            int(),
            Collector_adaptor<TestingCollector>::Result >;
    Signal sig;

    for(Sz i = 0; i < total_callbacks; ++i){
        sig.add( [](){ return cb_result; } );
    }
    sig.emit();
}

//----------- the end of CollectorTesting -----------//

//---------------- ExceptionTesting ----------------//

namespace { // anonymous ns

// an extended collector base that handles exceptions
template<typename Return_tp, typename Call_result_tp>
class Mock_collector_exc_base:
        public Mock_collector_base<Return_tp, Call_result_tp>
{
public:
    MOCK_METHOD(bool, handle, (std::exception_ptr));
};

// the matcher to check std::exception_ptr
template < typename T>
class Exception_Eq_matcher:
        public ::testing::MatcherInterface<std::exception_ptr>
{
public:
    Exception_Eq_matcher(T target): target_(std::move(target)) {}

    bool MatchAndExplain(
            std::exception_ptr p,
            ::testing::MatchResultListener* listener) const override
    {
        try {
            std::rethrow_exception(p);
        } catch (const T& obj) {
            if(target_ != obj) {
                *listener << "points to "<< obj;
                return false;
            } else {
                return true;
            }
        } catch (...) {
            *listener << "points to an object of unexcpected type";
            return false;
        }
    }

    void DescribeTo(std::ostream* os) const override {
        if(!os) return;
        *os << "points to " << target_;
    }

private:
    T target_;
};

template  <typename T>
::testing::Matcher<std::exception_ptr> ExcEq(T&& target){
    using Value = std::remove_reference_t<T>;
    return ::testing::MakeMatcher<std::exception_ptr>(
                new Exception_Eq_matcher<Value>(std::forward<T>(target)));
}

}; // anonymous ns

/*
 * Tests few things, that are too small for separate tests
 *  1) if method handle(...) is called if a callback throws
 *  2) if method handle(...) is called if a collect(...) throws
 *  3) if an exception thrown from get() may be caught
 */
TEST(SignalCollector, exceptionHandling) {
    const int expected_exc_value = 3;

    class TestingCollector: public Mock_collector_exc_base<void, int>
    {
    public:
        TestingCollector() {
            ExpectationSet es;

            es += EXPECT_CALL(*this, collect(0) ).WillOnce(Return(true));
            es += EXPECT_CALL(*this, handle(ExcEq(1))).WillOnce(Return(true));

            auto&& e = EXPECT_CALL(*this, collect(2) ).WillOnce(Throw(2));
            es += e;
            es += EXPECT_CALL(*this, handle(ExcEq(2))).After(e).WillOnce(Return(true));

            EXPECT_CALL(*this, get()).After(es).WillOnce(Throw(expected_exc_value));
        }
    };

    using Signal =
        dk::Signal<
            int(),
            Collector_adaptor<TestingCollector>::Result >;
    Signal sig;

    sig.add( [](){ return 0; } );
    sig.add( []() -> int { throw 1; } );
    sig.add( [](){ return 2; } );

    try {
        sig.emit();
        ADD_FAILURE() << "Signal::emit hasn't throw an exception";
    } catch (int& exception_value) {
        EXPECT_EQ(exception_value, expected_exc_value );
    } catch (...) {
        ADD_FAILURE() << "unexpected exception";
    }
}

// tests the case when handle(...) returns false
TEST(SignalCollector, exceptionBreaksLoop) {
    class TestingCollector: public Mock_collector_exc_base<void, int>
    {
    public:
        TestingCollector() {
            Sequence seq;
            EXPECT_CALL(*this, collect(0) ).InSequence(seq).WillOnce(Return(true));
            EXPECT_CALL(*this, handle(ExcEq(1))).InSequence(seq).WillOnce(Return(false));
            EXPECT_CALL(*this, get()).InSequence(seq);
        }
    };

    using Signal =
        dk::Signal<
            int(),
            Collector_adaptor<TestingCollector>::Result >;
    Signal sig;

    sig.add( [](){ return 0; } );
    sig.add( []() -> int { throw 1; } );
    sig.add( [](){ return 2; } );
    sig.add( [](){ return 3; } );
    sig.emit();
}

//----------- the end of ExceptionTesting -----------//

} //  namespace dk_unit_test

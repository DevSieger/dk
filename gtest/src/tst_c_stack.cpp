/*
 * Copyright (c) 2014-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <algorithm>
#include <dk/concurrency/stack.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <tuple>
#include <vector>
#include <thread>
#include <future>
#include <shared_mutex>
#include <condition_variable>
#include <algorithm>

using ::testing::Test;
using ::testing::Values;

namespace dk_unit_test {

namespace  {

using Size  = std::size_t;

class ConcurrentStackStressTestSuite: public ::testing::TestWithParam<Size>
{
public:
    Size num_threads() const {
        Size t  = std::thread::hardware_concurrency();
        if( t == 0 ) {
            t   = 4;
        }
        return t;
    }

    void launch(const std::vector<std::function<void()>>& fns){
        std::vector<std::future<void>> results;

        std::atomic_size_t n_ready(0);

        for( auto&& f: fns){
            auto wrapped    = [&f, &n_ready,&fns](){
                ++n_ready;

                // IMO it is ok to use "hot" locking here.
                while(n_ready.load(std::memory_order_relaxed) < fns.size())
                    ;

                f();
            };
            results.emplace_back(std::async(std::launch::async, std::move(wrapped)));
        }

        for(auto&& f: results){
            f.wait();
        }
    }

    template<typename Fn_factory>
    void generic_processing(const Fn_factory& fc){
        std::vector<std::function<void()>> fns;
        Size n_threads  = num_threads();
        Size total      = GetParam();
        using TVec      = std::vector<Size>;

        std::vector<TVec> in(n_threads);
        std::vector<TVec> out(n_threads);


        dk::Concurrent_stack<Size> stack;

        Size cur    = 0;
        for(Size i = 0; i < n_threads; ++i){
            Size n_tasks    = total / n_threads + ((total % n_threads) < i);

            auto& lin   = in[i];
            auto& lout  = out[i];

            for(Size j = 0; j < n_tasks; ++j){
                lin.push_back(cur++);
            }

            fns.push_back( fc(stack, lin, lout) );
        }

        launch(fns);

        TVec united;
        for(Size i = 0; i < n_threads; ++i){
            united.insert(united.end(), out[i].cbegin(), out[i].cend());
        }
        std::sort(united.begin(), united.end());

        for(Size i = 0; i < total; ++i){
            EXPECT_EQ( united[i], i );
        }
    }
};

}

INSTANTIATE_TEST_SUITE_P(,ConcurrentStackStressTestSuite, Values( 0x100, 0x10000, 0x100000 ));

TEST_P(ConcurrentStackStressTestSuite, emplaceAndPop){

        auto factory    = [](auto&& stack, auto&& lin, auto&& lout){
            return [&stack, &lin, &lout](){
                while(!lin.empty()){
                    Size value  = lin.back();
                    stack.emplace(value);
                    lin.pop_back();
                    auto res    = stack.pop();
                    EXPECT_TRUE(static_cast<bool>(res));
                    lout.push_back(res.value());
                }
            };
        };
        generic_processing(factory);
}

TEST_P(ConcurrentStackStressTestSuite, pushRangeAndPop){

        auto factory    = [](auto&& stack, auto&& lin, auto&& lout){
            return [&stack, &lin, &lout](){
                while(!lin.empty()){
                    Size count  = (lin.size() > 5)? 5: lin.size();
                    auto last   = lin.cend();
                    auto first  = last  - static_cast<std::ptrdiff_t>(count);

                    stack.push_range(first, last);
                    lin.erase(first, last);

                    for(Size i = 0; i < count; ++i){
                        auto res    = stack.pop();
                        EXPECT_TRUE(static_cast<bool>(res));
                        lout.push_back(res.value());
                    }
                }
            };
        };
        generic_processing(factory);
}

TEST(ConcurrentStack, empty){
    dk::Concurrent_stack<Size> stack;
    EXPECT_TRUE(stack.empty());
    stack.emplace(1);
    EXPECT_FALSE(stack.empty());
}

TEST(ConcurrentStack, pop){
    dk::Concurrent_stack<Size> stack;
    Size value = 1;
    stack.emplace(value);
    auto res    = stack.pop();
    EXPECT_TRUE(res);
    EXPECT_EQ(res.value(), value);
    res         = stack.pop();
    EXPECT_FALSE(res);
}

TEST(ConcurrentStack, clear){
    dk::Concurrent_stack<Size> stack;
    stack.emplace(1);
    stack.clear();
    EXPECT_TRUE(stack.empty());
}

}

# DK

DK - is a set of helpful (and not so helpful) classes that I'm using in development.
I made it public because they are required in my other small projects, like demos.
Some of them are ugly, and were written long time ago, don't judge me harshly :)

---

Things that may be intresting for other people:

* format_string.hpp - Yet another attempt to make type safe printf. Features: it gives the user an ability to add custom formatting parameters.
* signal.hpp - Yet another signal class. It is designed for the case of an recursive signal emission (I don't like a behaviour of boost::signals2: the observer of the signal may see signal invocations in the wrong order).
* comparable.hpp - A class that generates comparison operators. Features: it respects a behaviour of implicit conversions in the "source" operator.

---

The stuff in the folder "legacy" is a subject to be removed in future (it is used in my old-old code).
